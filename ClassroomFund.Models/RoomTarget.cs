﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ClassroomFund.Models
{
    /// <summary>
    /// Room targets
    /// </summary>
    public partial class RoomTarget
    {
        public RoomTarget()
        {
            Rooms = new HashSet<Room>();
        }

        public int Id { get; set; }
        public string TargetName { get; set; }
        
        [IgnoreDataMember]
        public virtual ICollection<Room> Rooms { get; set; }
    }
}
