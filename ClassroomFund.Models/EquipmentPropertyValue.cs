﻿using System;

namespace ClassroomFund.Models;

public class EquipmentPropertyValue
{
    public Guid Id { get; set; }
    public Guid EquipmentPropertyId { get; set; }
    
    public string Value { get; set; }
    public int ConcreteEquipmentId { get; set; }

    public virtual ConcreteEquipment Equipment { get; set; }
    public virtual EquipmentProperty EquipmentProperty { get; set; }
}
