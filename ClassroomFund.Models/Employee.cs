﻿using System;
using System.Collections.Generic;

namespace ClassroomFund.Models
{
    public class Employee
    {
        public Employee()
        {
            ClassroomMovementHistories = new HashSet<ClassroomMovementHistory>();
            Schedules = new HashSet<Schedule>();
            TicketAssignees = new HashSet<Ticket>();
            TicketReporterNavigations = new HashSet<Ticket>();
            TicketTypes = new HashSet<TicketType>();
        }

        public Guid Id { get; set; }
        public string FullName { get; set; }
        public int? EmployeeTitleId { get; set; }
        
        public int? DepartmentId { get; set; }
        
        public Department Department { get; set; }
        
        public Department MyDepartment { get; set; }
        
        public int? MyDepartmentId { get; set; }

        public EmployeeTitle EmployeeTitle { get; set; }
        public ICollection<ClassroomMovementHistory> ClassroomMovementHistories { get; set; }
        public ICollection<Schedule> Schedules { get; set; }
        public ICollection<Ticket> TicketAssignees { get; set; }
        public ICollection<Ticket> TicketReporterNavigations { get; set; }
        public ICollection<TicketType> TicketTypes { get; set; }
    }
}
