﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ClassroomFund.Models
{
    public class EquipmentType
    {
        public EquipmentType()
        {
            ConcreteEquipments = new HashSet<ConcreteEquipment>();
        }

        public Guid Id { get; set; }
        
        public string TypeName { get; set; }
        
        [NotMapped]
        public string PropertiesString {
            get
            {
                if(PropertiesForType is not { Count: 0 })
                    return "Пусто";
                StringBuilder builder = new StringBuilder();
                foreach (var prop in PropertiesForType)
                {
                    builder.Append(prop.Name);
                    builder.Append("; ");
                }
                return builder.ToString();
            } 
        }

        public ICollection<ConcreteEquipment> ConcreteEquipments { get; set; }
        
        public ICollection<EquipmentProperty> PropertiesForType { get; set; }
    }
}
