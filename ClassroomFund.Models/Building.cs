﻿using System;
using System.Collections.Generic;

namespace ClassroomFund.Models
{
    public partial class Building
    {
        public Building()
        {
            Departments = new HashSet<Department>();
            FloorInfoes = new HashSet<FloorInfo>();
            Rooms = new HashSet<Room>();
            ScheduleTimes = new HashSet<ScheduleTime>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }

        public virtual ICollection<Department> Departments { get; set; }
        public virtual ICollection<FloorInfo> FloorInfoes { get; set; }
        public virtual ICollection<Room> Rooms { get; set; }
        public virtual ICollection<ScheduleTime> ScheduleTimes { get; set; }
    }
}
