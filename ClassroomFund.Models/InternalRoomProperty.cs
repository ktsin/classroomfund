﻿using System;
using System.Collections.Generic;

namespace ClassroomFund.Models
{
    /// <summary>
    /// Some values regarding internal properties of room
    /// </summary>
    public class InternalRoomProperty
    {
        public int Id { get; set; }
        
        public Guid? RoomId { get; set; }
        public string Ceiling { get; set; }
        public string Walls { get; set; }
        public string WindowsType { get; set; }
        public string FloorMaterial { get; set; }
        public string Doors { get; set; }
        public int? LampsCount { get; set; }
        public string SanitaryEquipment { get; set; }

        public virtual Room Room { get; set; }
    }
}
