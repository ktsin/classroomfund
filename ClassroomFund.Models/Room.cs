﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace ClassroomFund.Models
{
    /// <summary>
    /// Room description
    /// </summary>
    public partial class Room
    {
        public Room()
        {
            ClassroomMovementHistories = new HashSet<ClassroomMovementHistory>();
            ConcreteEquipments = new HashSet<ConcreteEquipment>();
            Schedules = new HashSet<Schedule>();
        }

        public Guid Id { get; set; }
        
        public Guid FloorId { get; set; }
        public string RoomNumber { get; set; }
        
        public int RoomFloor { get; set; }
        public Guid? RoomTypeId { get; set; }
        public Guid? BuildingId { get; set; }
        public int? Seats { get; set; }
        public double? Area { get; set; }

        /// <summary>
        /// Date of last maintenance
        /// </summary>
        public DateOnly? LastMaintenance { get; set; }
        public int? RoomTargetId { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        public Building Building { get; set; }
        
        public RoomTarget RoomTarget { get; set; }
        public RoomType RoomType { get; set; }
        
        public FloorInfo FloorInfo { get; set; }
        public ICollection<ClassroomMovementHistory> ClassroomMovementHistories { get; set; }
        public ICollection<ConcreteEquipment> ConcreteEquipments { get; set; }
        
        public int? DepartmentId { get; set; }
        
        public Department Department { get; set; }
        public InternalRoomProperty InternalRoomProperties { get; set; }
        public ICollection<Schedule> Schedules { get; set; }
    }
}
