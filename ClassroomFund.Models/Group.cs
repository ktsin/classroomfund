﻿using System;
using System.Collections.Generic;

namespace ClassroomFund.Models
{
    public partial class Group
    {
        public Group()
        {
            Schedules = new HashSet<Schedule>();
        }

        public Guid Id { get; set; }
        public string GroupName { get; set; }
        public int NumberOfStudents { get; set; }
        public int SubGroupsNumber { get; set; }

        public virtual ICollection<Schedule> Schedules { get; set; }
    }
}
