﻿using System;
using System.Collections.Generic;

namespace ClassroomFund.Models
{
    public partial class Schedule
    {
        public Guid Id { get; set; }
        public Guid GroupId { get; set; }
        public Guid RoomId { get; set; }
        public Guid? TutorId { get; set; }
        public int ScheduleTimeId { get; set; }
        public int TypeId { get; set; }
        public int ForSubGroup { get; set; }
        public int? WeekType { get; set; }

        public virtual Group Group { get; set; }
        public virtual Room Room { get; set; }
        public virtual ScheduleTime ScheduleTime { get; set; }
        public virtual Employee Tutor { get; set; }
        public virtual ScheduleRecordType Type { get; set; }
    }
}
