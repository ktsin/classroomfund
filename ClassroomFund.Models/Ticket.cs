﻿using System;
using System.Collections.Generic;

namespace ClassroomFund.Models
{
    public partial class Ticket
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Guid? Reporter { get; set; }
        public DateTime Opened { get; set; }
        public DateTime LastUpdate { get; set; }
        public bool Resolved { get; set; }
        public DateTime? ResolvedAt { get; set; }
        public Guid? AssigneeId { get; set; }

        public virtual Employee Assignee { get; set; }
        public virtual Employee ReporterNavigation { get; set; }
    }
}
