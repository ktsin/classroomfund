﻿using System;
using System.Collections.Generic;

namespace ClassroomFund.Models
{
    public partial class TicketType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Guid? NotifyFirst { get; set; }

        public virtual Employee NotifyFirstNavigation { get; set; }
    }
}
