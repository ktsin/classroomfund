﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace ClassroomFund.Models
{
    public partial class FloorInfo
    {
        public Guid Id { get; set; }
        public Guid Building { get; set; }
        public int FloorNumber { get; set; }
        public string FloorImagePath { get; set; }
        public string ClassroomToImageMapping { get; set; }
        
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual Building BuildingNavigation { get; set; }
        
        public virtual ICollection<Room> Rooms { get; set; }
    }
}
