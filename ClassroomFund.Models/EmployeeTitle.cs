﻿using System;
using System.Collections.Generic;

namespace ClassroomFund.Models
{
    public partial class EmployeeTitle
    {
        public EmployeeTitle()
        {
            Employees = new HashSet<Employee>();
        }

        public int Id { get; set; }
        public string TitleName { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }
    }
}
