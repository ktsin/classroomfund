﻿using System;
using System.Collections.Generic;

namespace ClassroomFund.Models
{
    public partial class ConcreteEquipment
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        
        public DateTime YearOfAcceptance { get; set; }
        public string CountingNumber { get; set; }
        public int Count { get; set; }

        public Guid TypeId { get; set; }
        public Guid RoomId { get; set; }

        public virtual Room Room { get; set; }
        public virtual EquipmentType Type { get; set; }
        
        public virtual ICollection<EquipmentPropertyValue> Properies { get; set; }
    }
}
