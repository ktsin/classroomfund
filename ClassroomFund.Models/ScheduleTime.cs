﻿using System;
using System.Collections.Generic;

namespace ClassroomFund.Models
{
    public partial class ScheduleTime
    {
        public ScheduleTime()
        {
            Schedules = new HashSet<Schedule>();
        }

        public int Id { get; set; }
        public Guid BuildingId { get; set; }
        public int LessonNumber { get; set; }
        public DateTimeOffset LessonStart { get; set; }
        public DateTimeOffset LessonEnd { get; set; }

        public virtual Building Building { get; set; }
        public virtual ICollection<Schedule> Schedules { get; set; }
    }
}
