﻿using System;
using System.Collections.Generic;

namespace ClassroomFund.Models
{
    public class Department
    {
        public Department()
        {
            ClassroomMovementHistoryFromDepartmentNavigations = new HashSet<ClassroomMovementHistory>();
            ClassroomMovementHistoryToDepartmentNavigations = new HashSet<ClassroomMovementHistory>();
        }

        public int Id { get; set; }
        public string DepartmentName { get; set; }
        public Guid? HeadOfDepartmentId { get; set; }
        
        public Guid? BuildingId { get; set; }
        public Guid? RoomOfHead { get; set; }

        public virtual List<Building> Buildings { get; set; }
        public virtual Employee HeadOfDepartment { get; set; }
        public virtual Room RoomOfHeadNavigation { get; set; }
        
        public virtual List<Room> Rooms { get; set; }
        public virtual ICollection<ClassroomMovementHistory> ClassroomMovementHistoryFromDepartmentNavigations { get; set; }
        public virtual ICollection<ClassroomMovementHistory> ClassroomMovementHistoryToDepartmentNavigations { get; set; }
    }
}
