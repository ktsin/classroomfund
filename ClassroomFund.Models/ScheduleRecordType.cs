﻿using System;
using System.Collections.Generic;

namespace ClassroomFund.Models
{
    public partial class ScheduleRecordType
    {
        public ScheduleRecordType()
        {
            Schedules = new HashSet<Schedule>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Schedule> Schedules { get; set; }
    }
}
