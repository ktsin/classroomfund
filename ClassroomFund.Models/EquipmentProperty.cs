﻿using System;
using System.Collections.Generic;

namespace ClassroomFund.Models;

public class EquipmentProperty
{
    public Guid Id { get; set; }
    
    public string Name { get; set; }
    
    public string Type { get; set; }
    
    public ICollection<EquipmentType> EquipmentTypes { get; set; }
}
