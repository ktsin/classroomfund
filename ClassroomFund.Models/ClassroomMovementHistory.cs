﻿using System;
using System.Collections.Generic;

namespace ClassroomFund.Models
{
    /// <summary>
    /// History of movement classrooms between faculties
    /// </summary>
    public class ClassroomMovementHistory
    {
        public Guid Id { get; set; }
        public Guid? ApprovedBy { get; set; }
        public DateTime EventDate { get; set; }
        public int FromDepartment { get; set; }
        public int ToDepartment { get; set; }
        public Guid ClassroomId { get; set; }

        public Employee ApprovedByNavigation { get; set; }
        public Room Classroom { get; set; }
        public Department FromDepartmentNavigation { get; set; }
        public Department ToDepartmentNavigation { get; set; }
    }
}
