﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ClassroomFund.Models;

public class ImageMapping
{
    public string ObjectId { get; set; } = String.Empty;

    public List<Point> ImageBounds { get; set; } = new();
}

public class Point
{
    [JsonPropertyName("x")]
    public double X { get; set; }

    [JsonPropertyName("y")]
    public double Y { get; set; }
}
