﻿using System;
using System.Collections.Generic;

namespace ClassroomFund.Models
{
    public partial class GroupLoad
    {
        public Guid? GroupId { get; set; }
        public Guid? EmployeeId { get; set; }
        public int? HoursCount { get; set; }
        public int? SubGroupNumber { get; set; }
        public int? LessonTypeId { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Group Group { get; set; }
        public virtual ScheduleRecordType LessonType { get; set; }
    }
}
