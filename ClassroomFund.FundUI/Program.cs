using System.Text.Encodings.Web;
using System.Text.Unicode;
using ClassroomFund.ScheduleUI.Infrastructure;
using Microsoft.EntityFrameworkCore;
using ClassroomFund.ScheduleUI.Models;
using ClassroomFund.ScheduleUI.Models.Users;
using Syncfusion.Licensing;
using ClassroomFund.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Internal;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;

var builder = WebApplication.CreateBuilder(args);
var connectionString = builder.Configuration.GetConnectionString("UserContextConnection");

builder.Services.AddDbContext<UserContext>(options =>
    {
    options.UseNpgsql(connectionString);
    options.UseLoggerFactory(NullLoggerFactory.Instance);
    options.UseMemoryCache(new MemoryCache(new OptionsWrapper<MemoryCacheOptions>(new MemoryCacheOptions()
    {
        Clock = new SystemClock(),
        ExpirationScanFrequency = TimeSpan.FromSeconds(3600),
        SizeLimit = (long?)4e7
    })));
    }, ServiceLifetime.Singleton, ServiceLifetime.Singleton);

builder.Services.AddDefaultIdentity<ApplicationUser>(options =>
        {
        options.SignIn.RequireConfirmedAccount = true;
        options.Password.RequireLowercase = false;
        options.Password.RequireUppercase = false;
        options.Password.RequireNonAlphanumeric = false;
        options.Password.RequiredLength = 8;
        })
    .AddRoles<IdentityRole>()
    .AddEntityFrameworkStores<UserContext>()
    .AddUserManager<InternalUserManager>();
// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.ConfigureServiceLayer(builder.Configuration);
builder.Services.AddSingleton(HtmlEncoder.Create(allowedRanges: new[]
{
    UnicodeRanges.BasicLatin, UnicodeRanges.Cyrillic
}));
builder.WebHost.UseIISIntegration();


var app = builder.Build();


SyncfusionLicenseProvider.RegisterLicense(Environment.GetEnvironmentVariable("SFLK"));

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles(new StaticFileOptions()
{
    ServeUnknownFileTypes = true,
    HttpsCompression = HttpsCompressionMode.Compress,
    OnPrepareResponse = context =>
        {
        context.Context.Response.Headers[HeaderNames.CacheControl] =
            $"public,max-age={60 * 60 * 24 * 7}";
        }
});

app.UseRouting();
app.UseAuthentication();

app.UseAuthorization();

app.MapControllerRoute(
name: "default",
pattern: "{controller=Home}/{action=Index}/{id?}");
app.MapRazorPages();

app.UseFastReport();
await CreateRoles(app.Services, app.Configuration);
app.Run();

async Task CreateRoles(IServiceProvider serviceProvider, IConfiguration configuration)
{
    using var serviceScope = app.Services.CreateScope();
    serviceProvider = serviceScope.ServiceProvider;

    var userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();
    var roleManager = serviceProvider.GetService<RoleManager<IdentityRole>>();

    string[] roleNames = RolesConstants.GetRoles();
    
    IdentityResult roleResult;

    foreach (var roleName in roleNames)
    {
        var roleExist = await roleManager.RoleExistsAsync(roleName);
        if (!roleExist)
        {
            roleResult = await roleManager.CreateAsync(new IdentityRole(roleName));
        }
    }

    //Here you could create a super user who will maintain the web app
    var poweruser = new ApplicationUser
    {
        FullName = "Администратор",
        UserName = configuration["AppSettings:AdminUserEmail"],
        Email = configuration["AppSettings:AdminUserEmail"],
        EmailConfirmed = true
    };

    string userPwd = configuration["AppSettings:AdminUserPassword"];
    var user = await userManager.FindByEmailAsync(configuration["AppSettings:AdminUserEmail"]);

    if (user == null)
    {
        var createPowerUser = await userManager.CreateAsync(poweruser, userPwd);
        if (createPowerUser.Succeeded)
        {
            await userManager.AddToRoleAsync(poweruser, "Admin");
        }
    }
}

void AddHierarchy( AuthorizationOptions options)
{
    
}