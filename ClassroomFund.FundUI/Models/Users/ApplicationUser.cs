﻿using Microsoft.AspNetCore.Identity;

namespace ClassroomFund.ScheduleUI.Models.Users;

public class ApplicationUser : IdentityUser
{
    public string? FullName { get; set; }
    
    public Guid? EmployeeId { get; set; }
    
    public bool IsAccessAllowed { get; set; }
}
