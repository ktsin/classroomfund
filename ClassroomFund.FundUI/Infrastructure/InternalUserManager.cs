﻿using ClassroomFund.ScheduleUI.Models;
using ClassroomFund.ScheduleUI.Models.Users;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;

namespace ClassroomFund.ScheduleUI.Infrastructure;

public class InternalUserManager : UserManager<ApplicationUser>
{
    
    
    public InternalUserManager(
        IUserStore<ApplicationUser> store,
        IOptions<IdentityOptions> optionsAccessor,
        IPasswordHasher<ApplicationUser> passwordHasher,
        IEnumerable<IUserValidator<ApplicationUser>> userValidators,
        IEnumerable<IPasswordValidator<ApplicationUser>> passwordValidators,
        ILookupNormalizer keyNormalizer,
        IdentityErrorDescriber errors,
        IServiceProvider services,
        ILogger<UserManager<ApplicationUser>> logger) 
        : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
    {
    }
    
    public override async Task<IdentityResult> AddToRoleAsync(ApplicationUser user, string role)
    {
        ThrowIfDisposed();
        if (user == null)
        {
            throw new ArgumentNullException(nameof(user));
        }

        var normalizedRole = NormalizeName(role);
        if (!await base.IsInRoleAsync(user, normalizedRole))
        {
            if (normalizedRole == RolesConstants.AdminRole.ToUpperInvariant())
            {
                foreach (var roleName in RolesConstants.GetRoles().Where(e=>e is not (RolesConstants.AdminRole or RolesConstants.BasicRole)))
                {
                    await base.AddToRoleAsync(user, roleName);
                }
            }
            await base.AddToRoleAsync(user, normalizedRole);
            
        }
        return await UpdateUserAsync(user);
    }
}