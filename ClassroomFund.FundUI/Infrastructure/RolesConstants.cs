﻿namespace ClassroomFund.ScheduleUI.Infrastructure;

public static class RolesConstants
{
    public const string BasicRole = "Basic";
    public const string SecretaryRole = "Secretary";
    public const string EngineerRole = "Engineer";
    public const string RectorRole = "Rector";
    public const string AdminRole = "Admin";

    public const string BasicPolicy = "Basic";
    public const string SecretaryPolicy = "Secretary";
    public const string EngineerPolicy = "Engineer";
    public const string RectorPolicy = "Rector";
    public const string AdminPolicy = "Admin";
    
    public static string[] GetRoles()
    {
        return new[]
        {
            BasicRole, SecretaryRole, EngineerRole, RectorRole, AdminRole
        };
    }
}
