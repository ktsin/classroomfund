﻿using Microsoft.AspNetCore.Mvc;

namespace ClassroomFund.ScheduleUI.ViewComponents
{
    public class DrawMapViewComponent : ViewComponent
    {
        private readonly string _mapId;
        private List<int> _poly;
        private string _imageUrl;

        public DrawMapViewComponent()
        {

        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View("Default.cshtml");
        }
    }
}
