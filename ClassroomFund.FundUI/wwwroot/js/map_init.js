﻿if (map !== undefined) {
    map = undefined;
}
var map = L.map('map').setView([52.422677, 31.017404], 10);
// L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
//     attribution: '',
//     maxZoom: 18,
//     id: 'mapbox/streets-v11',
//     tileSize: 512,
//     zoomOffset: -1,
//     accessToken: 'pk.eyJ1Ijoia3RzaW4iLCJhIjoiY2wydWZ2OXZjMDFudjNrbGIycGo0bmIwMSJ9.yvKnVzD5Yw9qm4FzQwFUzA'
// }).addTo(map);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{}).addTo(map);

function searchAndFill(queryValue) {
    queryValue = encodeURIComponent(queryValue);
    $.ajax("https://nominatim.openstreetmap.org/search?q=" + queryValue + "&format=json").then((response) => {
        let lat = response[0].lat;
        let lon = response[0].lon;
        L.marker(response[0]).addTo(map);
        map.setView(response[0], 15);
    })
    
}
