﻿const observerOptions = {
    childList: true
}

const observerCallback = mutations =>{
    if(typeof htmx !== 'undefined'){
        htmx.process(document.body);
    }
}

const changesObserver = new MutationObserver(observerCallback);

changesObserver.observe(document.body, observerOptions);