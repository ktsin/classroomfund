﻿var ajax = new ej.base.Ajax('/locale/ru.json', 'GET', false);
ajax.send().then((e) => {
    var loader = JSON.parse(e);
    ej.base.L10n.load(
        loader
    );
    ej.base.setCulture('ru');
});

loadCultureFiles('ru');

function loadCultureFiles(name) {
    var files = ['ca-gregorian.json', 'numberingSystems.json', 'numbers.json', 'timeZoneNames.json', 'ca-islamic.json'];
    var loader = ej.base.loadCldr;
    var loadCulture = function (prop) {
        var val, ajax;
        if (files[prop] === 'numberingSystems.json') {
            ajax = new ej.base.Ajax('/cldr-data/supplemental/' + files[prop], 'GET', false);
        } else {
            ajax = new ej.base.Ajax('/cldr-data/main/' + name + '/' + files[prop], 'GET', false);
        }
        ajax.onSuccess = function (value) {
            val = value;
        };
        ajax.send();
        loader(JSON.parse(val));
    };
    for (var prop = 0; prop < files.length; prop++) {
        loadCulture(prop);
    }
}

ej.base.setCurrencyCode('BYN');