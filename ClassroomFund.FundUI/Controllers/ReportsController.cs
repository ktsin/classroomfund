﻿using System.ComponentModel.DataAnnotations;
using ClassroomFund.Services.Reports;
using Microsoft.AspNetCore.Mvc;

namespace ClassroomFund.ScheduleUI.Controllers;

public class ReportsController : Controller
{
    private readonly ReportsService _reportsService;

    public ReportsController(ReportsService reportsService)
    {
        _reportsService = reportsService;
    }

    public async Task<IActionResult> EquipmentByDepartment([Required, FromRoute] int id)
    {
        var reportContent = await _reportsService.EquipmentInDepartment(id);
        
        return File(reportContent, "application/pdf");
    }
}