﻿using System.ComponentModel.DataAnnotations;
using ClassroomFund.Models;
using ClassroomFund.Services.Classrooms.Interfaces;
using ClassroomFund.Services.Departments;
using ClassroomFund.Services.Relocate;
using Microsoft.AspNetCore.Mvc;

namespace ClassroomFund.ScheduleUI.Controllers;

public class BatchRelocateController : Controller
{
    private readonly IRoomService _roomService;
    private readonly IRelocateService _relocateService;
    private readonly IDepartmentsService _departmentsService;

    public BatchRelocateController(IRoomService roomService, IRelocateService relocateService,
        IDepartmentsService departmentsService)
    {
        _roomService = roomService;
        _relocateService = relocateService;
        _departmentsService = departmentsService;
    }

    // GET
    public IActionResult Index()
    {
        return View();
    }

    public async Task<IActionResult> Relocate([Required] Guid roomId)
    {
        var room = await _roomService.GetById(roomId);
        if (room == null)
        {
            return NotFound();
        }

        var department = await _departmentsService.GetById(room.DepartmentId ?? 0);
        var depList = (await _departmentsService.GetAll()).Where(e => e.Id != department.Id).ToList();

        var model = new RelocateModel()
        {
            TargetRoom = room,
            SourceDepartment = department,
            ListOfDepartments = depList,
            NewDepartmentId = -1
        };
        return View(model);
    }

    public IActionResult RelocatePost()
    {
        return RedirectToAction("Index");
    }
}

public class RelocateModel
{
    public Room TargetRoom { get; set; }
    
    public Guid RoomId { get; set; }

    public Department SourceDepartment { get; set; }

    public List<Department> ListOfDepartments { get; set; }

    public int NewDepartmentId { get; set; }
    
    public string RedirectUrl { get; set; }
}