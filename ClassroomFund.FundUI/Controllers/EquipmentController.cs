﻿using ClassroomFund.Services.Classrooms.Interfaces;
using ClassroomFund.Services.Equipment.Interface;
using Microsoft.AspNetCore.Mvc;

namespace ClassroomFund.ScheduleUI.Controllers;

public class EquipmentController : Controller
{
    private readonly IEquipmentPropertiesService _equipmentPropertiesService;
    private readonly IEquipmentTypeService _equipmentTypeService;
    private readonly IRoomService _roomService;

    public EquipmentController(IEquipmentPropertiesService equipmentPropertiesService, IEquipmentTypeService equipmentTypeService, IRoomService roomService)
    {
        _equipmentPropertiesService = equipmentPropertiesService;
        _equipmentTypeService = equipmentTypeService;
        _roomService = roomService;
    }
    
    public IActionResult Index()
    {
        return View();
    }

    public IActionResult Edit([FromRoute] Guid id)
    {
        
    }
}
