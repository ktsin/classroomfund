﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json;
using ClassroomFund.Models;
using ClassroomFund.Services.Classrooms.Interfaces;
using ClassroomFund.Services.Departments;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ClassroomFund.ScheduleUI.Controllers;

public class RoomController : Controller
{
    private readonly IRoomService _roomsService;
    private readonly IBuildingService _buildingService;
    private readonly IDepartmentsService _departmentsService;
    private readonly IFloorInfoService _floorInfoService;

    public RoomController(IRoomService roomsService, IBuildingService buildingService, IDepartmentsService departmentsService, IFloorInfoService floorInfoService)
    {
        _floorInfoService = floorInfoService;
        _departmentsService = departmentsService;
        _buildingService = buildingService;
        _roomsService = roomsService;
    }

    public async Task<IActionResult> Index()
    {
        RoomIndexViewModel model = new();
        var rooms = model.Rooms = await _roomsService.GetAll();
        var buildings = await _buildingService.GetAllBuildings();
        var departments = await _departmentsService.GetAll();
        model.RoomsTree = new List<object>();
        model.RoomsTree.AddRange(buildings.Select(e => new
        {
            e.Id,
            e.Name,
            HasChild = true,
            IsRoot = true
        }));
        model.RoomsTree.AddRange(rooms.Select((e) => new
        {
            e.Id,
            e.BuildingId,
            Name = e.RoomNumber,
            IsRoot = false
        }).OrderBy(e => e.Name));
        model.DepartmentsTree = new List<object>();
        model.DepartmentsTree.AddRange(departments.Select(e => new
        {
            e.Id,
            Name = e.DepartmentName,
            IsRoot = true,
            HasChild = true
        }));
        model.DepartmentsTree.AddRange(rooms.Select((e) => new
        {
            e.Id,
            DepartmentId = e.DepartmentId,
            Name = e.RoomNumber,
            IsRoot = false,
            HasChild = false
        }).OrderBy(e => e.Name));
        return View(model);
    }

    public async Task<IActionResult> Overview([Required]Guid id)
    {
        var info = await _roomsService.GetByIdWithInfo(id);
        RoomOverviewViewModel model = new();
        model.RoomInfo = info;
        return View(model);
    }

    public async Task<IActionResult> OverviewEmbed([Required]Guid id)
    {
        var info = await _roomsService.GetByIdWithInfo(id);
        RoomOverviewViewModel model = new();
        model.RoomInfo = info;
        return PartialView("Overview", model);
    }

    public async Task<IActionResult> Edit(Guid id)
    {
        var info = await _roomsService.GetByIdWithInfo(id);
        if (info == null)
        {
            return NotFound();
        }
        return View();
    }

    public async Task<IActionResult> AddRoomFirstStep(Guid floorId)
    {
        ViewBag.FloorId = floorId;
        var floor = await _floorInfoService.GetById(floorId);
        if (floor == null)
        {
            return NotFound();
        }
        return View(floor);
    }

    [HttpPost]
    public async Task<IActionResult> AddRoomSecondStep([FromForm] Guid floorId, [FromForm] string roomMap)
    {
        if (String.IsNullOrWhiteSpace(roomMap))
        {
            roomMap = "[]";
        }
        Point[] points = JsonSerializer.Deserialize<Point[]>(roomMap);

        AddRoomSecondStepModel model = new AddRoomSecondStepModel();
        model.FloorId = floorId;
        model.RoomMap = points;
        model.RoomTypes = (await _roomsService.GetRoomTypes())
            .Select(e => new SelectListItem(e.Name, e.Id.ToString()))
            .ToArray();
        model.RoomTargets = (await _roomsService.GetRoomTargets())
            .Select(e => new SelectListItem(e.TargetName, e.Id.ToString()))
            .ToArray();
        model.Departments = (await _departmentsService.GetAll())
            .Select(e => new SelectListItem(e.DepartmentName, e.Id.ToString()))
            .ToArray();
        model.FloorInfo = await _floorInfoService.GetByIdWithInfo(floorId);
        return View(model);
    }

    [HttpPost]
    public async Task<IActionResult> AddRoomComplete(AddRoomSecondStepModel model, [FromForm] string roomMap)
    {
        //model.NewRoom.FloorId = model.FloorId;
        var room = await _roomsService.AddRoom(model.NewRoom);
        var properties = await _roomsService.AddRoomInformation(model.NewRoomProperties);
        //TODO: add mapping insert
        await _floorInfoService.AddImageMapping(model.NewRoom.FloorId, model.NewRoom.Id, JsonSerializer.Deserialize<Point[]>( roomMap));
        if (model.SaveAndGoBack)
        {
            return RedirectToAction("Overview", "Floors" , model.FloorId);
        }
        else
        {
            return RedirectToAction("Overview", room?.Id);
        }
    }
}

public class RoomOverviewViewModel
{
    public Room RoomInfo { get; set; }
}

public class RoomIndexViewModel
{
    public List<Room> Rooms { get; set; }

    public List<Building> Buildings { get; set; }

    public List<Department> Departments { get; set; }

    public List<object> RoomsTree { get; set; }

    public List<object> DepartmentsTree { get; set; }
}

public class RoomEditViewModel
{
    public Room Target { get; set; }

    public List<RoomTarget> RoomTargets { get; set; }

    public List<RoomType> RoomTypes { get; set; }

    public FloorInfo Floor { get; set; }
}

public class AddRoomSecondStepModel
{
    public Guid FloorId { get; set; }
    
    public Point[] RoomMap { get; set; }

    public Guid NewRoomId { get; set; } = Guid.NewGuid();

    public Room NewRoom { get; set; }
    
    public FloorInfo FloorInfo { get; set; }
    
    public InternalRoomProperty NewRoomProperties { get; set; }

    public SelectListItem[] RoomTypes { get; set; }
    
    public SelectListItem[] RoomTargets { get; set; }
    
    public SelectListItem[] Departments { get; set; }
    
    public bool SaveAndGoBack { get; set; }
}
