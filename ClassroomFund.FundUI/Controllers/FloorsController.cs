﻿using System.ComponentModel.DataAnnotations;
using ClassroomFund.Models;
using ClassroomFund.Services.Classrooms.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ClassroomFund.ScheduleUI.Controllers;

public class FloorsController : Controller
{
    private readonly IFloorInfoService _floorInfo;
    private readonly IRoomService _roomService;
    private readonly IBuildingService _buildingService;

    public FloorsController(IFloorInfoService floorInfo, IRoomService roomService, IBuildingService buildingService)
    {
        _floorInfo = floorInfo;
        _roomService = roomService;
        _buildingService = buildingService;
    }

    public async Task<IActionResult> Overview([Required] Guid id)
    {
        var floor = await _floorInfo.GetByIdWithInfo(id);
        OverviewViewModel model = new OverviewViewModel();
        model.FloorInfo = floor;
        if (floor != null)
        {
            model.Rooms = await _roomService.GetRoomsByFloorId(id);
            return View(model);
        }

        return NotFound();
    }

    public async Task<IActionResult> AddFloorFirstStep(Guid buildingId)
    {
        var building = await _buildingService.GetBuildingById(buildingId);
        if (building == null)
        {
            return NotFound();
        }

        var model = new AddFloorFirstStepViewModel();
        model.Building = building;
        model.LatestFloorNumber = await _buildingService.GetLastFloorNumber(buildingId) + 1;
        model.FloorInfo = new()
        {
            FloorNumber = model.LatestFloorNumber,
            Building = buildingId,
            ClassroomToImageMapping = "[]",
            Id = Guid.NewGuid()
        };
        return View(model);
    }

    [HttpPost]
    public async Task<IActionResult> AddFloorPost([FromForm] AddFloorFirstStepViewModel model)
    {
        await _floorInfo.AddFloor(model.FloorInfo);
        return Redirect(model.RedirectToUrl);
    }
}

public class OverviewViewModel
{
    public FloorInfo FloorInfo { get; set; }

    public List<Room>? Rooms { get; set; }
}

public class AddFloorFirstStepViewModel
{
    public Building? Building { get; set; }

    public int LatestFloorNumber { get; set; }

    public FloorInfo? FloorInfo { get; set; }

    public string RedirectToUrl { get; set; }
}