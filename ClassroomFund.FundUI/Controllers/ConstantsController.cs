﻿using ClassroomFund.ScheduleUI.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ClassroomFund.ScheduleUI.Controllers;

[Authorize(Roles = RolesConstants.AdminRole)]
public class ConstantsController : Controller
{
    // GET
    public IActionResult Index()
    {
        return View();
    }
}
