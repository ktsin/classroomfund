﻿using ClassroomFund.Models;
using ClassroomFund.Services.Equipment.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ClassroomFund.ScheduleUI.Controllers;

public class EquipmentTypeController : Controller
{
    private readonly IEquipmentTypeService _equipmentTypeService;
    private readonly IEquipmentPropertiesService _equipmentPropertiesService;

    public EquipmentTypeController(IEquipmentTypeService equipmentTypeService,
        IEquipmentPropertiesService equipmentPropertiesService)
    {
        _equipmentTypeService = equipmentTypeService;
        _equipmentPropertiesService = equipmentPropertiesService;
    }

    public IActionResult Index()
    {
        return View();
    }

    public async Task<IActionResult> Edit([FromRoute] Guid id)
    {
        var type = await _equipmentTypeService.GetByIdWithInfo(id);
        var properties = await _equipmentPropertiesService.GetAll();
        var selectList = properties.Select(e => new SelectListItem(e.Name, e.Id.ToString(), false))
            .ToList();
        if (type.PropertiesForType != null)
        {
            foreach (var property in type.PropertiesForType)
            {
                var element = selectList.FirstOrDefault(e => e.Value == property.Id.ToString());
                if (element != null)
                    element.Selected = true;
            }
        }

        var model = new EditPropertyModalModel()
        {
            EquipmentType = type,
            Options = selectList
        };

        return PartialView(model);
    }
    
    [HttpPost("[action]")]
    public async Task<IActionResult> EditPost([FromRoute] Guid id, string[] props)
    {
        
        return RedirectToAction("Edit", new {id});
    }
}



public class EditPropertyModalModel
{
    public EquipmentType EquipmentType { get; set; }

    public List<SelectListItem> Options { get; set; }
}