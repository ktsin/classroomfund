﻿using ClassroomFund.Models;
using ClassroomFund.Services.Classrooms.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ClassroomFund.ScheduleUI.Controllers
{
    public class BuildingsController : Controller
    {
        private readonly IBuildingService _buildingService;

        public BuildingsController(IBuildingService buildingService)
        {
            _buildingService = buildingService;
        }

        // GET: BuildingsController
        public async Task<ActionResult> Index()
        {
            var model = new MainModel();
            model.Buildings = await _buildingService.GetAllBuildingsWithInfo();
            model.BuildingNumber = model.Buildings?.Count ?? 0;
            model.RoomsNumber = 42;
            model.TotalArea = 4294.2345678;


            return View(model);
        }

        public async Task<ActionResult> BuildingOverview(Guid id)
        {
            Building building = await _buildingService.GetByIdWithInfo(id);
            return PartialView(building);
        }
    }

    public class MainModel
    {
        public List<Building>? Buildings { get; set; }

        public int BuildingNumber { get; set; }

        public int RoomsNumber { get; set; }

        public double TotalArea { get; set; }
    }
}
