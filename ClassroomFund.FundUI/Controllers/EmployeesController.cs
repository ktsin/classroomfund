﻿using ClassroomFund.Models;
using ClassroomFund.Services.UserManagment.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ClassroomFund.ScheduleUI.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly IEmployeeService _employeeService;

        public EmployeesController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        public async Task<IActionResult> Index()
        {
            EmployeesIndexViewModel model = new();
            model.Employees = await _employeeService.GetAll();
            model.Titles = await _employeeService.GetAllTitles();
            return View(model);
        }
    }

    public class EmployeesIndexViewModel
    {
        public List<Employee> Employees { get; set; }

        public List<EmployeeTitle> Titles { get; set; }
    }
}
