﻿using System.Text.Json;
using ClassroomFund.Models;
using ClassroomFund.ScheduleUI.Infrastructure;
using ClassroomFund.Services.Departments;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ClassroomFund.ScheduleUI.Controllers.API;

[Route("/api/Departments")]
public class DepartmentsApiController : Controller
{
    private readonly IDepartmentsService _departmentsService;
    
    public DepartmentsApiController(IDepartmentsService departmentsService)
    {
        _departmentsService = departmentsService;
    }
    
    [HttpGet]
    public async Task<IActionResult> List()
    {
        var list = await _departmentsService.GetAll();
        var response = new
        {
            Items = list,
            Count = list?.Count
        };
        return Json(response, new JsonSerializerOptions
        {
            PropertyNamingPolicy = null
        });
    }

    [HttpPost]
    public async Task<IActionResult> AddDepartment([FromBody]Department department)
    {
        var result = await _departmentsService.Add(department);
        return Json(result);
    }

    [HttpPut]
    public async Task<IActionResult> UpdateDepartment([FromBody] Department department)
    {
        var result = await _departmentsService.Add(department);
        return Json(result);
    }
    
    [HttpPut]
    public async Task<IActionResult> RemoveDepartment([FromRoute]int id)
    {
        // var result = await _departmentsService.Add(null);
        return StatusCode(200);
    }
}
