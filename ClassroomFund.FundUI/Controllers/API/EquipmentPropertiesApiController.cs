﻿using System.Text.Json;
using ClassroomFund.Models;
using ClassroomFund.Services.Equipment.Interface;
using Microsoft.AspNetCore.Mvc;

namespace ClassroomFund.ScheduleUI.Controllers.API;

[Route("/api/[controller]")]
public class EquipmentPropertiesApiController : Controller
{
    private readonly IEquipmentPropertiesService _equipmentPropertiesService;
    
    private readonly JsonSerializerOptions _serializerOptions = new()
    {
        PropertyNamingPolicy = null
    };

    public EquipmentPropertiesApiController(IEquipmentPropertiesService equipmentPropertiesService)
    {
        _equipmentPropertiesService = equipmentPropertiesService;
    }

    [HttpGet("/api/[controller]")]
    public async Task<IActionResult> GetAll()
    {
        if (!Request.Query.ContainsKey("$select"))
        {
            var items = await _equipmentPropertiesService.GetAll();
            var result = new
            {
                Items = items,
                Count = items?.Count ?? 0
            };
            return Json(result, _serializerOptions);
        }
        else
        {
            var variants  = new[]
            {
                "string", "number", "datetime", "boolean"
            };
            var result = new
            {
                Items = variants,
                Count = variants.Length
            };
            return Json(result, _serializerOptions);
        }
    }
    
    [HttpPost("/api/[controller]")]
    public async Task<IActionResult> Create([FromBody] EquipmentProperty equipmentProperty)
    {
        var result = await _equipmentPropertiesService.Add(equipmentProperty);
        return Json(result, _serializerOptions);
    }
}
