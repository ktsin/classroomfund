﻿using System.Text.Json;
using ClassroomFund.Services.FileOperations.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ClassroomFund.ScheduleUI.Controllers.API;

[Route("/api/Images/")]
public class ImageApiController : Controller
{
    private readonly IImageService _imageService;
    private readonly JsonSerializerOptions _options = new ()
    {
        PropertyNamingPolicy = null
    };

    public ImageApiController(IImageService imageService)
    {
        _imageService = imageService;
    }

    [HttpPost("[action]")]
    public async Task<IActionResult> Upload([FromForm] IFormFile mainFile)
    {
        string url = await _imageService.AddImage(mainFile.OpenReadStream());
        //url = Uri.UnescapeDataString(url);
        return Json(new {Url =  url});
    }
    
    [HttpPut("[action]")]
    public async Task<IActionResult> Delete([FromBody] string url)
    {
        await _imageService.RemoveImage(url);
        return Ok();
    }
}