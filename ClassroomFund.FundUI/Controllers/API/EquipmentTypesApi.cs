﻿using System.Text.Json;
using ClassroomFund.Models;
using ClassroomFund.Services.Equipment.Interface;
using Microsoft.AspNetCore.Mvc;

namespace ClassroomFund.ScheduleUI.Controllers.API;

[Route("/api/[controller]")]
public class EquipmentTypesApi : Controller
{
    private readonly IEquipmentTypeService _equipmentTypeService;
    
    private readonly JsonSerializerOptions _serializerOptions = new()
    {
        PropertyNamingPolicy = null
    };

    public EquipmentTypesApi(IEquipmentTypeService equipmentTypeService)
    {
        _equipmentTypeService = equipmentTypeService;
    }

    [HttpGet("/api/[controller]")]
    public async Task<IActionResult> GetAll()
    {
        var items = await _equipmentTypeService.GetAll();
        var result = new
        {
            Items = items,
            Count = items?.Count ?? 0
        };
        return Json(result, _serializerOptions);
    }
    
    [HttpPost("/api/[controller]")]
    public async Task<IActionResult> Add([FromBody] EquipmentType equipmentType)
    {
        var result = await _equipmentTypeService.Add(equipmentType);
        return Json(result, _serializerOptions);
    }
}
