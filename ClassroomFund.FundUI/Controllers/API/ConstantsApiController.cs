﻿using System.Text.Json;
using ClassroomFund.Models;
using ClassroomFund.Services.Classrooms.Interfaces;
using ClassroomFund.Services.UserManagment.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ClassroomFund.ScheduleUI.Controllers.API;

[Route("/api/Constants")]
public class ConstantsApiController : Controller
{
    private readonly IEmployeeService _employeeService;
    private readonly IRoomService _roomService;

    private readonly JsonSerializerOptions serializerOptions = new()
    {
        PropertyNamingPolicy = null
    };
    
    public ConstantsApiController(IEmployeeService employeeService, IRoomService roomService)
    {
        _roomService = roomService;
        _employeeService = employeeService;
    }

    #region EmployeeTitle

    [HttpGet("EmployeeTitles")]
    public async Task<IActionResult> GetEmployeeTitles()
    {
        var list = await _employeeService.GetAllTitles();
        var response = new
        {
            Items = list,
            Count = list.Count
        };
        return Json(response, serializerOptions);
    }
    
    [HttpPost("EmployeeTitles")]
    public async Task<IActionResult> PostEmployeeTitles(EmployeeTitle record)
    {
        if (!String.IsNullOrWhiteSpace(record.TitleName))
        {
            return Json(await _employeeService.AddEmployeeTitle(record), serializerOptions);
        }
        else
        {
            return StatusCode(400);
        }
    }
    
    [HttpPut("EmployeeTitles")]
    public async Task<IActionResult> PutEmployeeTitles(EmployeeTitle record)
    {
        return View();
    }
    
    [HttpDelete("EmployeeTitles")]
    public async Task<IActionResult> DeleteEmployeeTitles(EmployeeTitle record)
    {
        return View();
    }
    
    #endregion

    #region RoomType

    [HttpGet("RoomTypes")]
    public async Task<IActionResult> GetRoomTypes()
    {
        var list = await _roomService.GetRoomTypes();
        var response = new
        {
            Items = list,
            Count = list.Count
        };
        return Json(response, serializerOptions);
    }
    
    [HttpPost("RoomTypes")]
    public async Task<IActionResult> PostRoomTypes([FromBody] RoomType record)
    {
        if (!String.IsNullOrWhiteSpace(record.Name))
        {
            return Json(await _roomService.AddRoomType(record), serializerOptions);
        }
        else
        {
            return StatusCode(400);
        }
    }
    
    [HttpPut("RoomTypes")]
    public async Task<IActionResult> PutRoomTypes(RoomType record)
    {
        return View();
    }
    
    [HttpDelete("RoomTypes")]
    public async Task<IActionResult> DeleteRoomTypes(RoomType record)
    {
        return View();
    }

    #endregion

    #region RoomTarget

    [HttpGet("RoomTargets")]
    public async Task<IActionResult> GetRoomTargets()
    {
        var list = await _roomService.GetRoomTargets();
        var response = new
        {
            Items = list,
            Count = list.Count
        };
        return Json(response, serializerOptions);
    }
    
    [HttpPost("RoomTargets")]
    public async Task<IActionResult> PostRoomTargets([FromBody] RoomTarget record)
    {
        if (!String.IsNullOrWhiteSpace(record.TargetName))
        {
            return Json(await _roomService.AddRoomTarget(record), serializerOptions);
        }
        else
        {
            return StatusCode(400);
        }
    }
    
    [HttpPut("RoomTargets")]
    public async Task<IActionResult> PutRoomTargets(RoomTarget record)
    {
        return View();
    }
    
    [HttpDelete("RoomTargets")]
    public async Task<IActionResult> DeleteRoomTargets(RoomTarget record)
    {
        return View();
    }

    #endregion
}
