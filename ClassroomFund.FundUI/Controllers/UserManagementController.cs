﻿using ClassroomFund.Models;
using ClassroomFund.ScheduleUI.Models;
using ClassroomFund.ScheduleUI.Models.Users;
using ClassroomFund.Services.UserManagment.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MimeKit.Cryptography;

namespace ClassroomFund.ScheduleUI.Controllers;

[Authorize(Roles = "Admin")]
public class UserManagementController : Controller
{
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly RoleManager<IdentityRole> _roleManager;
    private readonly IEmployeeService _employeeService;

    public UserManagementController(UserManager<ApplicationUser> userManager,
        RoleManager<IdentityRole> roleManager,
        IEmployeeService employeeService)
    {
        _employeeService = employeeService;
        _userManager = userManager;
        _roleManager = roleManager;
    } 

    // GET
    public async Task<IActionResult> Index()
    {
        return View();
    }

    public async Task<IActionResult> GrantAndFill(Guid id)
    {
        var user = await _userManager.FindByIdAsync(id.ToString("D"));
        if (user is null or {IsAccessAllowed: true})
        {
            return NotFound();
        }
        GrantAndFillViewModel model = new GrantAndFillViewModel();
        model.UIUser = user;
        model.EmployeeTitles = (await _employeeService.GetAllTitles())
            .Select(e => 
                new SelectListItem(e?.TitleName, e?.Id.ToString()))
            .ToArray();
        model.Roles = _roleManager.Roles.Select(e => new SelectListItem(e.Name, e.Id)).ToArray();
        return View(model);
    }
}

public class GrantAndFillViewModel
{
    public ApplicationUser UIUser { get; set; }

    public Employee ProfileModel { get; set; }
    
    public SelectListItem[] EmployeeTitles { get; set; }
    
    public SelectListItem[] Roles { get; set; }
}
