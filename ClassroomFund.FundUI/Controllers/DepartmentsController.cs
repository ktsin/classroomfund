﻿using ClassroomFund.Models;
using ClassroomFund.ScheduleUI.Infrastructure;
using ClassroomFund.ScheduleUI.Models.Users;
using ClassroomFund.Services.Departments;
using ClassroomFund.Services.UserManagment;
using ClassroomFund.Services.UserManagment.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NuGet.Protocol;

namespace ClassroomFund.ScheduleUI.Controllers;

[Authorize(Roles = $"{RolesConstants.AdminRole},{RolesConstants.EngineerRole},{RolesConstants.SecretaryRole}")]
public class DepartmentsController : Controller
{
    private readonly IDepartmentsService _departmentsService;
    private readonly IEmployeeService _employeeService;
    private readonly UserManager<ApplicationUser> _userManager;

    public DepartmentsController(IDepartmentsService departmentsService, IEmployeeService employeeService, 
        UserManager<ApplicationUser> userManager)
    {
        _departmentsService = departmentsService;
        _employeeService = employeeService;
        _userManager = userManager;
    }

    public IActionResult Index()
    {
        return View();
    }

    [Route("/[controller]/{id:int}")]
    public async Task<IActionResult> Overview([FromRoute] int id)
    {
        var model = await _departmentsService.GetByIdWithInfo(id);
        if (model == null)
        {
            return NotFound();
        }
        return View(model);
    }

    [Route("/[controller]/{id:int}/[action]")]
    public async Task<IActionResult> Edit([FromRoute] int id)
    {
        EditViewModel model = new();
        model.Department = await _departmentsService.GetByIdWithInfo(id);
        if (model.Department == null)
        {
            return NotFound();
        }
        return View(model);
    }

    public async Task<IActionResult> MyDepartmentOverview()
    {
        var user = await _userManager.GetUserAsync(this.User);
        var profile = await _employeeService.GetById(user.EmployeeId ?? Guid.Empty);
        if (profile == null)
        {
            return NotFound();
        }
        else
        {
            return RedirectToAction("Overview", new {id = profile.DepartmentId});
        }
    }

    public class EditViewModel
    {
        public Department Department { get; set; }
    }
}


