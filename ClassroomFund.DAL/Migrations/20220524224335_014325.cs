﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ClassroomFund.DAL.Migrations
{
    public partial class _014325 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "internalroomproperties_room_id_fk",
                table: "InternalRoomProperties");

            migrationBuilder.AddForeignKey(
                name: "internalroomproperties_room_id_fk",
                table: "InternalRoomProperties",
                column: "RoomId",
                principalTable: "Room",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "internalroomproperties_room_id_fk",
                table: "InternalRoomProperties");

            migrationBuilder.AddForeignKey(
                name: "internalroomproperties_room_id_fk",
                table: "InternalRoomProperties",
                column: "RoomId",
                principalTable: "Room",
                principalColumn: "Id");
        }
    }
}
