﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ClassroomFund.DAL.Migrations
{
    public partial class _04 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "classroommovementhistory_department_id_fk",
                table: "ClassroomMovementHistory");

            migrationBuilder.DropForeignKey(
                name: "classroommovementhistory_department_id_fk_2",
                table: "ClassroomMovementHistory");

            migrationBuilder.DropForeignKey(
                name: "classroommovementhistory_room_id_fk",
                table: "ClassroomMovementHistory");

            migrationBuilder.AlterColumn<Guid>(
                name: "ApprovedBy",
                table: "ClassroomMovementHistory",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddForeignKey(
                name: "classroommovementhistory_department_id_fk",
                table: "ClassroomMovementHistory",
                column: "FromDepartment",
                principalTable: "Department",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "classroommovementhistory_department_id_fk_2",
                table: "ClassroomMovementHistory",
                column: "ToDepartment",
                principalTable: "Department",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "classroommovementhistory_room_id_fk",
                table: "ClassroomMovementHistory",
                column: "ClassroomId",
                principalTable: "Room",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "classroommovementhistory_department_id_fk",
                table: "ClassroomMovementHistory");

            migrationBuilder.DropForeignKey(
                name: "classroommovementhistory_department_id_fk_2",
                table: "ClassroomMovementHistory");

            migrationBuilder.DropForeignKey(
                name: "classroommovementhistory_room_id_fk",
                table: "ClassroomMovementHistory");

            migrationBuilder.AlterColumn<Guid>(
                name: "ApprovedBy",
                table: "ClassroomMovementHistory",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "classroommovementhistory_department_id_fk",
                table: "ClassroomMovementHistory",
                column: "FromDepartment",
                principalTable: "Department",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "classroommovementhistory_department_id_fk_2",
                table: "ClassroomMovementHistory",
                column: "ToDepartment",
                principalTable: "Department",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "classroommovementhistory_room_id_fk",
                table: "ClassroomMovementHistory",
                column: "ClassroomId",
                principalTable: "Room",
                principalColumn: "Id");
        }
    }
}
