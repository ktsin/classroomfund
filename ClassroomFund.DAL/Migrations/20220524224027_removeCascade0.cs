﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ClassroomFund.DAL.Migrations
{
    public partial class removeCascade0 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FloorInfo_Room",
                table: "Room");

            migrationBuilder.AddForeignKey(
                name: "FK_FloorInfo_Room",
                table: "Room",
                columns: new[] { "FloorId", "RoomFloor" },
                principalTable: "FloorInfo",
                principalColumns: new[] { "Id", "FloorNumber" },
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FloorInfo_Room",
                table: "Room");

            migrationBuilder.AddForeignKey(
                name: "FK_FloorInfo_Room",
                table: "Room",
                columns: new[] { "FloorId", "RoomFloor" },
                principalTable: "FloorInfo",
                principalColumns: new[] { "Id", "FloorNumber" },
                onDelete: ReferentialAction.SetNull);
        }
    }
}
