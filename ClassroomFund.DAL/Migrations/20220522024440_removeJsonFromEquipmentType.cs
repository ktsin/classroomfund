﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ClassroomFund.DAL.Migrations
{
    public partial class removeJsonFromEquipmentType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Properties",
                table: "ConcreteEquipment");

            migrationBuilder.CreateTable(
                name: "EquipmentProperties",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Type = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EquipmentProperties", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EquipmentPropertyEquipmentType",
                columns: table => new
                {
                    EquipmentTypesId = table.Column<Guid>(type: "uuid", nullable: false),
                    PropertiesForTypeId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EquipmentPropertyEquipmentType", x => new { x.EquipmentTypesId, x.PropertiesForTypeId });
                    table.ForeignKey(
                        name: "FK_EquipmentPropertyEquipmentType_EquipmentProperties_Properti~",
                        column: x => x.PropertiesForTypeId,
                        principalTable: "EquipmentProperties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EquipmentPropertyEquipmentType_EquipmentType_EquipmentTypes~",
                        column: x => x.EquipmentTypesId,
                        principalTable: "EquipmentType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EquipmentPropertyValues",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    EquipmentPropertyId = table.Column<Guid>(type: "uuid", nullable: false),
                    Value = table.Column<string>(type: "text", nullable: true),
                    ConcreteEquipmentId = table.Column<int>(type: "integer", nullable: false),
                    EquipmentId = table.Column<Guid>(type: "uuid", nullable: true),
                    ConcreteEquipmentId1 = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EquipmentPropertyValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EquipmentPropertyValues_ConcreteEquipment_ConcreteEquipment~",
                        column: x => x.ConcreteEquipmentId1,
                        principalTable: "ConcreteEquipment",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_EquipmentPropertyValues_ConcreteEquipment_EquipmentId",
                        column: x => x.EquipmentId,
                        principalTable: "ConcreteEquipment",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_EquipmentPropertyValues_EquipmentProperties_EquipmentProper~",
                        column: x => x.EquipmentPropertyId,
                        principalTable: "EquipmentProperties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EquipmentPropertyEquipmentType_PropertiesForTypeId",
                table: "EquipmentPropertyEquipmentType",
                column: "PropertiesForTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_EquipmentPropertyValues_ConcreteEquipmentId1",
                table: "EquipmentPropertyValues",
                column: "ConcreteEquipmentId1");

            migrationBuilder.CreateIndex(
                name: "IX_EquipmentPropertyValues_EquipmentId",
                table: "EquipmentPropertyValues",
                column: "EquipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_EquipmentPropertyValues_EquipmentPropertyId",
                table: "EquipmentPropertyValues",
                column: "EquipmentPropertyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EquipmentPropertyEquipmentType");

            migrationBuilder.DropTable(
                name: "EquipmentPropertyValues");

            migrationBuilder.DropTable(
                name: "EquipmentProperties");

            migrationBuilder.AddColumn<string>(
                name: "Properties",
                table: "ConcreteEquipment",
                type: "json",
                nullable: true);
        }
    }
}
