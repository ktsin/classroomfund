﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ClassroomFund.DAL.Migrations
{
    public partial class _180501 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "Employee_EmployeeTitleId_fkey",
                table: "Employee");

            migrationBuilder.AlterColumn<int>(
                name: "EmployeeTitleId",
                table: "Employee",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "Employee_EmployeeTitleId_fkey",
                table: "Employee",
                column: "EmployeeTitleId",
                principalTable: "EmployeeTitle",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "Employee_EmployeeTitleId_fkey",
                table: "Employee");

            migrationBuilder.AlterColumn<int>(
                name: "EmployeeTitleId",
                table: "Employee",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "Employee_EmployeeTitleId_fkey",
                table: "Employee",
                column: "EmployeeTitleId",
                principalTable: "EmployeeTitle",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
