﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace ClassroomFund.DAL.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Building",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false, defaultValueSql: "'none'::character varying"),
                    Address = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: false, defaultValueSql: "'ул. Беды 2, г. Минск'::character varying")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Building", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeTitle",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityAlwaysColumn),
                    TitleName = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeTitle", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EquipmentType",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    TypeName = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EquipmentType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Group",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    GroupName = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: false, defaultValueSql: "'рога и олени'::character varying"),
                    NumberOfStudents = table.Column<int>(type: "integer", nullable: false, defaultValueSql: "1"),
                    SubGroupsNumber = table.Column<int>(type: "integer", nullable: false, defaultValueSql: "1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Group", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RoomTarget",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TargetName = table.Column<string>(type: "character varying", nullable: false, defaultValueSql: "'Неизвестно'::character varying")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomTarget", x => x.Id);
                },
                comment: "Room targets");

            migrationBuilder.CreateTable(
                name: "RoomTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ScheduleRecordType",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityAlwaysColumn),
                    Name = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScheduleRecordType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FloorInfo",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    FloorNumber = table.Column<int>(type: "integer", nullable: false),
                    Building = table.Column<Guid>(type: "uuid", nullable: false),
                    FloorImagePath = table.Column<string>(type: "character varying(1000)", maxLength: 1000, nullable: false),
                    ClassroomToImageMapping = table.Column<string>(type: "json", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("FloorInfo_pkey", x => new { x.Id, x.FloorNumber });
                    table.ForeignKey(
                        name: "FloorInfo_Building_fkey",
                        column: x => x.Building,
                        principalTable: "Building",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ScheduleTime",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityAlwaysColumn),
                    BuildingId = table.Column<Guid>(type: "uuid", nullable: false),
                    LessonNumber = table.Column<int>(type: "integer", nullable: false),
                    LessonStart = table.Column<DateTimeOffset>(type: "time with time zone", nullable: false),
                    LessonEnd = table.Column<DateTimeOffset>(type: "time with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScheduleTime", x => x.Id);
                    table.ForeignKey(
                        name: "ScheduleTime_BuildingId_fkey",
                        column: x => x.BuildingId,
                        principalTable: "Building",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Employee",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    FullName = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: false),
                    EmployeeTitleId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee", x => x.Id);
                    table.ForeignKey(
                        name: "Employee_EmployeeTitleId_fkey",
                        column: x => x.EmployeeTitleId,
                        principalTable: "EmployeeTitle",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Room",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    FloorId = table.Column<Guid>(type: "uuid", nullable: false),
                    RoomNumber = table.Column<string>(type: "character varying(5)", maxLength: 5, nullable: false),
                    RoomFloor = table.Column<int>(type: "integer", nullable: false),
                    RoomTypeId = table.Column<Guid>(type: "uuid", nullable: true),
                    BuildingId = table.Column<Guid>(type: "uuid", nullable: true),
                    seats = table.Column<int>(type: "integer", nullable: true, defaultValueSql: "'-1'::integer"),
                    area = table.Column<double>(type: "double precision", nullable: true, defaultValueSql: "0"),
                    LastMaintenance = table.Column<DateOnly>(type: "date", nullable: true, comment: "Date of last maintenance"),
                    RoomTargetId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Room", x => x.Id);
                    table.ForeignKey(
                        name: "BuildingFK",
                        column: x => x.BuildingId,
                        principalTable: "Building",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_FloorInfo_Room",
                        columns: x => new { x.FloorId, x.RoomFloor },
                        principalTable: "FloorInfo",
                        principalColumns: new[] { "Id", "FloorNumber" },
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "room_roomtarget_id_fk",
                        column: x => x.RoomTargetId,
                        principalTable: "RoomTarget",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "RoomTypeFK",
                        column: x => x.RoomTypeId,
                        principalTable: "RoomTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                },
                comment: "Room description");

            migrationBuilder.CreateTable(
                name: "GroupLoad",
                columns: table => new
                {
                    GroupId = table.Column<Guid>(type: "uuid", nullable: true),
                    EmployeeId = table.Column<Guid>(type: "uuid", nullable: true),
                    HoursCount = table.Column<int>(type: "integer", nullable: true),
                    SubGroupNumber = table.Column<int>(type: "integer", nullable: true),
                    LessonTypeId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.ForeignKey(
                        name: "groupload_employee_id_fk",
                        column: x => x.EmployeeId,
                        principalTable: "Employee",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "groupload_group_id_fk",
                        column: x => x.GroupId,
                        principalTable: "Group",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "groupload_schedulerecordtype_id_fk",
                        column: x => x.LessonTypeId,
                        principalTable: "ScheduleRecordType",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Ticket",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Title = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Reporter = table.Column<Guid>(type: "uuid", nullable: true),
                    Opened = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    LastUpdate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Resolved = table.Column<bool>(type: "boolean", nullable: false),
                    ResolvedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    AssigneeId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ticket", x => x.Id);
                    table.ForeignKey(
                        name: "Ticket_AssigneeId_fkey",
                        column: x => x.AssigneeId,
                        principalTable: "Employee",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "Ticket_Reporter_fkey",
                        column: x => x.Reporter,
                        principalTable: "Employee",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "TicketType",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityAlwaysColumn),
                    Name = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false),
                    NotifyFirst = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TicketType", x => x.Id);
                    table.ForeignKey(
                        name: "TicketType_NotifyFirst_fkey",
                        column: x => x.NotifyFirst,
                        principalTable: "Employee",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ConcreteEquipment",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: false),
                    YearOfAcceptance = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    CountingNumber = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true, defaultValueSql: "'000000'::character varying"),
                    Count = table.Column<int>(type: "integer", nullable: false, defaultValueSql: "1"),
                    Properties = table.Column<string>(type: "json", nullable: true),
                    TypeId = table.Column<Guid>(type: "uuid", nullable: false),
                    RoomId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConcreteEquipment", x => x.Id);
                    table.ForeignKey(
                        name: "ConcreteEquipment_RoomId_fkey",
                        column: x => x.RoomId,
                        principalTable: "Room",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "ConcreteEquipment_TypeId_fkey",
                        column: x => x.TypeId,
                        principalTable: "EquipmentType",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Department",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityAlwaysColumn),
                    DepartmentName = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false),
                    HeadOfDepartmentId = table.Column<Guid>(type: "uuid", nullable: true),
                    BuildingId = table.Column<Guid>(type: "uuid", nullable: true),
                    RoomOfHead = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Department", x => x.Id);
                    table.ForeignKey(
                        name: "Department_BuildingId_fkey",
                        column: x => x.BuildingId,
                        principalTable: "Building",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "Department_HeadOfDepartmentId_fkey",
                        column: x => x.HeadOfDepartmentId,
                        principalTable: "Employee",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "Department_RoomOfHead_fkey",
                        column: x => x.RoomOfHead,
                        principalTable: "Room",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "InternalRoomProperties",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RoomId = table.Column<Guid>(type: "uuid", nullable: true),
                    Ceiling = table.Column<string>(type: "character varying", nullable: true),
                    Walls = table.Column<string>(type: "character varying", nullable: true),
                    WindowsType = table.Column<string>(type: "character varying", nullable: true),
                    FloorMaterial = table.Column<string>(type: "character varying", nullable: true),
                    Doors = table.Column<string>(type: "character varying", nullable: true),
                    LampsCount = table.Column<int>(type: "integer", nullable: true, defaultValueSql: "0"),
                    SanitaryEquipment = table.Column<string>(type: "character varying", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InternalRoomProperties", x => x.Id);
                    table.ForeignKey(
                        name: "internalroomproperties_room_id_fk",
                        column: x => x.RoomId,
                        principalTable: "Room",
                        principalColumn: "Id");
                },
                comment: "Some values regarding internal properties of room");

            migrationBuilder.CreateTable(
                name: "Schedule",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    GroupId = table.Column<Guid>(type: "uuid", nullable: false),
                    RoomId = table.Column<Guid>(type: "uuid", nullable: false),
                    TutorId = table.Column<Guid>(type: "uuid", nullable: true),
                    ScheduleTimeId = table.Column<int>(type: "integer", nullable: false),
                    TypeId = table.Column<int>(type: "integer", nullable: false),
                    ForSubGroup = table.Column<int>(type: "integer", nullable: false, defaultValueSql: "'-1'::integer"),
                    WeekType = table.Column<int>(type: "integer", nullable: true, defaultValueSql: "0")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Schedule", x => x.Id);
                    table.ForeignKey(
                        name: "Schedule_GroupId_fkey",
                        column: x => x.GroupId,
                        principalTable: "Group",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "Schedule_RoomId_fkey",
                        column: x => x.RoomId,
                        principalTable: "Room",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "Schedule_ScheduleTimeId_fkey",
                        column: x => x.ScheduleTimeId,
                        principalTable: "ScheduleTime",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "Schedule_TutorId_fkey",
                        column: x => x.TutorId,
                        principalTable: "Employee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "Schedule_TypeId_fkey",
                        column: x => x.TypeId,
                        principalTable: "ScheduleRecordType",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ClassroomMovementHistory",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "gen_random_uuid()"),
                    ApprovedBy = table.Column<Guid>(type: "uuid", nullable: false),
                    EventDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, defaultValueSql: "now()"),
                    FromDepartment = table.Column<int>(type: "integer", nullable: false),
                    ToDepartment = table.Column<int>(type: "integer", nullable: false),
                    ClassroomId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassroomMovementHistory", x => x.Id);
                    table.ForeignKey(
                        name: "classroommovementhistory_department_id_fk",
                        column: x => x.FromDepartment,
                        principalTable: "Department",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "classroommovementhistory_department_id_fk_2",
                        column: x => x.ToDepartment,
                        principalTable: "Department",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "classroommovementhistory_employee_id_fk",
                        column: x => x.ApprovedBy,
                        principalTable: "Employee",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "classroommovementhistory_room_id_fk",
                        column: x => x.ClassroomId,
                        principalTable: "Room",
                        principalColumn: "Id");
                },
                comment: "History of movement classrooms between faculties");

            migrationBuilder.CreateIndex(
                name: "classroommovementhistory_classroomid_index",
                table: "ClassroomMovementHistory",
                column: "ClassroomId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassroomMovementHistory_ApprovedBy",
                table: "ClassroomMovementHistory",
                column: "ApprovedBy");

            migrationBuilder.CreateIndex(
                name: "IX_ClassroomMovementHistory_FromDepartment",
                table: "ClassroomMovementHistory",
                column: "FromDepartment");

            migrationBuilder.CreateIndex(
                name: "IX_ClassroomMovementHistory_ToDepartment",
                table: "ClassroomMovementHistory",
                column: "ToDepartment");

            migrationBuilder.CreateIndex(
                name: "IX_ConcreteEquipment_RoomId",
                table: "ConcreteEquipment",
                column: "RoomId");

            migrationBuilder.CreateIndex(
                name: "IX_ConcreteEquipment_TypeId",
                table: "ConcreteEquipment",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Department_BuildingId",
                table: "Department",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_Department_HeadOfDepartmentId",
                table: "Department",
                column: "HeadOfDepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Department_RoomOfHead",
                table: "Department",
                column: "RoomOfHead");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_EmployeeTitleId",
                table: "Employee",
                column: "EmployeeTitleId");

            migrationBuilder.CreateIndex(
                name: "IX_FloorInfo_Building",
                table: "FloorInfo",
                column: "Building");

            migrationBuilder.CreateIndex(
                name: "IX_GroupLoad_EmployeeId",
                table: "GroupLoad",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_GroupLoad_GroupId",
                table: "GroupLoad",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_GroupLoad_LessonTypeId",
                table: "GroupLoad",
                column: "LessonTypeId");

            migrationBuilder.CreateIndex(
                name: "internalroomproperties_ceiling_index",
                table: "InternalRoomProperties",
                column: "Ceiling");

            migrationBuilder.CreateIndex(
                name: "internalroomproperties_doors_index",
                table: "InternalRoomProperties",
                column: "Doors");

            migrationBuilder.CreateIndex(
                name: "internalroomproperties_floormaterial_index",
                table: "InternalRoomProperties",
                column: "FloorMaterial");

            migrationBuilder.CreateIndex(
                name: "internalroomproperties_sanitaryequipment_index",
                table: "InternalRoomProperties",
                column: "SanitaryEquipment");

            migrationBuilder.CreateIndex(
                name: "internalroomproperties_walls_index",
                table: "InternalRoomProperties",
                column: "Walls");

            migrationBuilder.CreateIndex(
                name: "internalroomproperties_windowstype_index",
                table: "InternalRoomProperties",
                column: "WindowsType");

            migrationBuilder.CreateIndex(
                name: "IX_InternalRoomProperties_RoomId",
                table: "InternalRoomProperties",
                column: "RoomId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "fki_BuildingFK",
                table: "Room",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "fki_RoomTypeFK",
                table: "Room",
                column: "RoomTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Room_FloorId_RoomFloor",
                table: "Room",
                columns: new[] { "FloorId", "RoomFloor" });

            migrationBuilder.CreateIndex(
                name: "IX_Room_RoomTargetId",
                table: "Room",
                column: "RoomTargetId");

            migrationBuilder.CreateIndex(
                name: "roomtarget_targetname_index",
                table: "RoomTarget",
                column: "TargetName");

            migrationBuilder.CreateIndex(
                name: "fk_schedule_room",
                table: "Schedule",
                column: "RoomId");

            migrationBuilder.CreateIndex(
                name: "IX_Schedule_GroupId",
                table: "Schedule",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Schedule_ScheduleTimeId",
                table: "Schedule",
                column: "ScheduleTimeId");

            migrationBuilder.CreateIndex(
                name: "IX_Schedule_TutorId",
                table: "Schedule",
                column: "TutorId");

            migrationBuilder.CreateIndex(
                name: "IX_Schedule_TypeId",
                table: "Schedule",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ScheduleTime_BuildingId",
                table: "ScheduleTime",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_Ticket_AssigneeId",
                table: "Ticket",
                column: "AssigneeId");

            migrationBuilder.CreateIndex(
                name: "IX_Ticket_Reporter",
                table: "Ticket",
                column: "Reporter");

            migrationBuilder.CreateIndex(
                name: "IX_TicketType_NotifyFirst",
                table: "TicketType",
                column: "NotifyFirst");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClassroomMovementHistory");

            migrationBuilder.DropTable(
                name: "ConcreteEquipment");

            migrationBuilder.DropTable(
                name: "GroupLoad");

            migrationBuilder.DropTable(
                name: "InternalRoomProperties");

            migrationBuilder.DropTable(
                name: "Schedule");

            migrationBuilder.DropTable(
                name: "Ticket");

            migrationBuilder.DropTable(
                name: "TicketType");

            migrationBuilder.DropTable(
                name: "Department");

            migrationBuilder.DropTable(
                name: "EquipmentType");

            migrationBuilder.DropTable(
                name: "Group");

            migrationBuilder.DropTable(
                name: "ScheduleTime");

            migrationBuilder.DropTable(
                name: "ScheduleRecordType");

            migrationBuilder.DropTable(
                name: "Employee");

            migrationBuilder.DropTable(
                name: "Room");

            migrationBuilder.DropTable(
                name: "EmployeeTitle");

            migrationBuilder.DropTable(
                name: "FloorInfo");

            migrationBuilder.DropTable(
                name: "RoomTarget");

            migrationBuilder.DropTable(
                name: "RoomTypes");

            migrationBuilder.DropTable(
                name: "Building");
        }
    }
}
