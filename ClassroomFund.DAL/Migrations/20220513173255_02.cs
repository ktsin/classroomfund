﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ClassroomFund.DAL.Migrations
{
    public partial class _02 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "Department_BuildingId_fkey",
                table: "Department");

            migrationBuilder.DropForeignKey(
                name: "Employee_EmployeeTitleId_fkey",
                table: "Employee");

            migrationBuilder.DropIndex(
                name: "IX_Department_BuildingId",
                table: "Department");

            migrationBuilder.AddColumn<int>(
                name: "DepartmentId",
                table: "Room",
                type: "integer",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "EmployeeTitleId",
                table: "Employee",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "BuildingDepartment",
                columns: table => new
                {
                    BuildingsId = table.Column<Guid>(type: "uuid", nullable: false),
                    DepartmentsId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingDepartment", x => new { x.BuildingsId, x.DepartmentsId });
                    table.ForeignKey(
                        name: "FK_BuildingDepartment_Building_BuildingsId",
                        column: x => x.BuildingsId,
                        principalTable: "Building",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BuildingDepartment_Department_DepartmentsId",
                        column: x => x.DepartmentsId,
                        principalTable: "Department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Room_DepartmentId",
                table: "Room",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_BuildingDepartment_DepartmentsId",
                table: "BuildingDepartment",
                column: "DepartmentsId");

            migrationBuilder.AddForeignKey(
                name: "Employee_EmployeeTitleId_fkey",
                table: "Employee",
                column: "EmployeeTitleId",
                principalTable: "EmployeeTitle",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Room_Department_DepartmentId",
                table: "Room",
                column: "DepartmentId",
                principalTable: "Department",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "Employee_EmployeeTitleId_fkey",
                table: "Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_Room_Department_DepartmentId",
                table: "Room");

            migrationBuilder.DropTable(
                name: "BuildingDepartment");

            migrationBuilder.DropIndex(
                name: "IX_Room_DepartmentId",
                table: "Room");

            migrationBuilder.DropColumn(
                name: "DepartmentId",
                table: "Room");

            migrationBuilder.AlterColumn<int>(
                name: "EmployeeTitleId",
                table: "Employee",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.CreateIndex(
                name: "IX_Department_BuildingId",
                table: "Department",
                column: "BuildingId");

            migrationBuilder.AddForeignKey(
                name: "Department_BuildingId_fkey",
                table: "Department",
                column: "BuildingId",
                principalTable: "Building",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "Employee_EmployeeTitleId_fkey",
                table: "Employee",
                column: "EmployeeTitleId",
                principalTable: "EmployeeTitle",
                principalColumn: "Id");
        }
    }
}
