﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ClassroomFund.DAL.Migrations
{
    public partial class _01350601 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "Department_HeadOfDepartmentId_fkey",
                table: "Department");

            migrationBuilder.DropIndex(
                name: "IX_Department_HeadOfDepartmentId",
                table: "Department");

            migrationBuilder.AddColumn<int>(
                name: "DepartmentId",
                table: "Employee",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MyDepartmentId",
                table: "Employee",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Employee_DepartmentId",
                table: "Employee",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_MyDepartmentId",
                table: "Employee",
                column: "MyDepartmentId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "Department_HeadOfDepartmentId_fkeyn",
                table: "Employee",
                column: "MyDepartmentId",
                principalTable: "Department",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_Department_DepartmentId",
                table: "Employee",
                column: "DepartmentId",
                principalTable: "Department",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "Department_HeadOfDepartmentId_fkeyn",
                table: "Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee_Department_DepartmentId",
                table: "Employee");

            migrationBuilder.DropIndex(
                name: "IX_Employee_DepartmentId",
                table: "Employee");

            migrationBuilder.DropIndex(
                name: "IX_Employee_MyDepartmentId",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "DepartmentId",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "MyDepartmentId",
                table: "Employee");

            migrationBuilder.CreateIndex(
                name: "IX_Department_HeadOfDepartmentId",
                table: "Department",
                column: "HeadOfDepartmentId");

            migrationBuilder.AddForeignKey(
                name: "Department_HeadOfDepartmentId_fkey",
                table: "Department",
                column: "HeadOfDepartmentId",
                principalTable: "Employee",
                principalColumn: "Id");
        }
    }
}
