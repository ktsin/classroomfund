﻿using ClassroomFund.DAL.Repository.Interfaces.Base;
using ClassroomFund.Models;

namespace ClassroomFund.DAL.Repository.Interfaces;

public interface IRelocationHistoryRepository : IRepository<ClassroomMovementHistory, Guid>
{
}