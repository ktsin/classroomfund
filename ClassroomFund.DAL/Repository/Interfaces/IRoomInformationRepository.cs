﻿using ClassroomFund.DAL.Repository.Database;
using ClassroomFund.DAL.Repository.Interfaces.Base;
using ClassroomFund.Models;

namespace ClassroomFund.DAL.Repository.Interfaces;

public interface IRoomInformationRepository : IRepository<InternalRoomProperty, int>
{
    Task<InternalRoomProperty> GetByRoomId(Guid id);
}
