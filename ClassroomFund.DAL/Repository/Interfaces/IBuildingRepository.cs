﻿using ClassroomFund.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassroomFund.DAL.Repository.Interfaces.Base;

namespace ClassroomFund.DAL.Repository.Interfaces
{
    public interface IBuildingRepository : IRepository<Building, Guid>
    {
    }
}
