﻿using ClassroomFund.DAL.Repository.Interfaces.Base;
using ClassroomFund.Models;

namespace ClassroomFund.DAL.Repository.Interfaces;

public interface IRoomTargetRepository : IRepository<RoomTarget, int>
{
    
}
