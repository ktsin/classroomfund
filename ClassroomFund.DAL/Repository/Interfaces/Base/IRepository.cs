﻿namespace ClassroomFund.DAL.Repository.Interfaces.Base
{
    public interface IRepository<T, in TU> where T : class
        where TU : struct
    {
        public Task<List<T>> GetAll();

        public Task<List<T>> GetAllWithJoin();

        public Task<T> GetById(TU id);

        public Task<T> GetByIdWithJoin(TU id);

        public Task<T> Add(T entity);

        public Task<T> Update(T entity);

        public Task Delete(TU id);
    }
}
