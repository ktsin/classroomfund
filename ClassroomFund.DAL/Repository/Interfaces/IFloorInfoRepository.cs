﻿using ClassroomFund.Models;
using ClassroomFund.DAL.Repository.Interfaces.Base;

namespace ClassroomFund.DAL.Repository.Interfaces
{
    public interface IFloorInfoRepository : IRepository<FloorInfo, Guid>
    {
        public Task<List<FloorInfo>> GetAllFloorsByBuildingId(Guid id);
    }
}
