﻿using ClassroomFund.DAL.Repository.Interfaces.Base;
using ClassroomFund.Models;

namespace ClassroomFund.DAL.Repository.Interfaces;

public interface IEmployeeTitleRepository : IRepository<EmployeeTitle, int>
{
    
}
