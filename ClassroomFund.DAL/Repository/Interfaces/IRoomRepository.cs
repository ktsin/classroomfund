﻿using ClassroomFund.DAL.Repository.Interfaces.Base;
using ClassroomFund.Models;

namespace ClassroomFund.DAL.Repository.Interfaces;

public interface IRoomRepository : IRepository<Room, Guid>
{
    public Task<List<Room>> GetRoomsByFloorId(Guid floorId);

    public Task<List<Room>> GetRoomsByBuildingId(Guid buildingId);

    public Task<List<Room>> GetRoomsByDepartmentId(int departmentId);

    public Task<List<Room>> GetRoomsByFloorIdWithJoin(Guid floorId);

    public Task<List<Room>> GetRoomsByBuildingIdWithJoin(Guid buildingId);

    public Task<List<Room>> GetRoomsByDepartmentIdWithJoin(int departmentId);
}
