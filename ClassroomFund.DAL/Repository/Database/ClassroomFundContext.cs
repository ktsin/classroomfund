﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using ClassroomFund.Models;

namespace ClassroomFund.DAL.Repository.Database
{
    public class ClassroomFundContext : DbContext
    {
        public virtual DbSet<Building> Buildings { get; set; }
        public virtual DbSet<ClassroomMovementHistory> ClassroomMovementHistories { get; set; }
        public virtual DbSet<ConcreteEquipment> ConcreteEquipments { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<EmployeeTitle> EmployeeTitles { get; set; }
        public virtual DbSet<EquipmentType> EquipmentTypes { get; set; }
        public virtual DbSet<FloorInfo> FloorInfoes { get; set; }
        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<GroupLoad> GroupLoads { get; set; }
        public virtual DbSet<InternalRoomProperty> InternalRoomProperties { get; set; }
        public virtual DbSet<Room> Rooms { get; set; }
        public virtual DbSet<RoomTarget> RoomTargets { get; set; }
        public virtual DbSet<RoomType> RoomTypes { get; set; }
        public virtual DbSet<Schedule> Schedules { get; set; }
        public virtual DbSet<ScheduleRecordType> ScheduleRecordTypes { get; set; }
        public virtual DbSet<ScheduleTime> ScheduleTimes { get; set; }
        public virtual DbSet<Ticket> Tickets { get; set; }
        public virtual DbSet<TicketType> TicketTypes { get; set; }
        public virtual DbSet<EquipmentProperty> EquipmentProperties { get; set; }
        public virtual DbSet<EquipmentPropertyValue> EquipmentPropertyValues { get; set; }

        public ClassroomFundContext(DbContextOptions<ClassroomFundContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Building>(entity =>
            {
                entity.ToTable("Building");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(500)
                    .HasDefaultValueSql("'ул. Беды 2, г. Минск'::character varying");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasDefaultValueSql("'none'::character varying");
            });

            modelBuilder.Entity<ClassroomMovementHistory>(entity =>
            {
                entity.ToTable("ClassroomMovementHistory");

                entity.HasComment("History of movement classrooms between faculties");

                entity.HasIndex(e => e.ClassroomId, "classroommovementhistory_classroomid_index");

                entity.Property(e => e.Id).HasDefaultValueSql("gen_random_uuid()");

                entity.Property(e => e.EventDate)
                    .HasColumnType("timestamp without time zone")
                    .HasDefaultValueSql("now()");

                entity.HasOne(d => d.ApprovedByNavigation)
                    .WithMany(p => p.ClassroomMovementHistories)
                    .HasForeignKey(d => d.ApprovedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("classroommovementhistory_employee_id_fk");

                entity.HasOne(d => d.Classroom)
                    .WithMany(p => p.ClassroomMovementHistories)
                    .HasForeignKey(d => d.ClassroomId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("classroommovementhistory_room_id_fk");

                entity.HasOne(d => d.FromDepartmentNavigation)
                    .WithMany(p => p.ClassroomMovementHistoryFromDepartmentNavigations)
                    .HasForeignKey(d => d.FromDepartment)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("classroommovementhistory_department_id_fk");

                entity.HasOne(d => d.ToDepartmentNavigation)
                    .WithMany(p => p.ClassroomMovementHistoryToDepartmentNavigations)
                    .HasForeignKey(d => d.ToDepartment)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("classroommovementhistory_department_id_fk_2");
            });

            modelBuilder.Entity<ConcreteEquipment>(entity =>
            {
                entity.ToTable("ConcreteEquipment");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Count).HasDefaultValueSql("1");

                entity.Property(e => e.CountingNumber)
                    .HasMaxLength(200)
                    .HasDefaultValueSql("'000000'::character varying");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.HasOne(d => d.Room)
                    .WithMany(p => p.ConcreteEquipments)
                    .HasForeignKey(d => d.RoomId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ConcreteEquipment_RoomId_fkey");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.ConcreteEquipments)
                    .HasForeignKey(d => d.TypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ConcreteEquipment_TypeId_fkey");

                entity.HasMany<EquipmentPropertyValue>()
                    .WithOne(e => e.Equipment);
            });


            modelBuilder.Entity<Department>(entity =>
            {
                entity.ToTable("Department");

                entity.Property(e => e.Id).UseIdentityAlwaysColumn();

                entity.Property(e => e.DepartmentName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.HasMany<Building>(d => d.Buildings)
                    .WithMany(p => p.Departments);

                entity.HasOne(d => d.HeadOfDepartment)
                    .WithOne(p => p.MyDepartment)
                    .HasForeignKey(nameof(Employee), "MyDepartmentId")
                    .HasConstraintName("Department_HeadOfDepartmentId_fkeyn");

                entity.HasMany<Employee>()
                    .WithOne(e => e.Department)
                    .HasForeignKey(e => e.DepartmentId);

                entity.HasOne(d => d.RoomOfHeadNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.RoomOfHead)
                    .HasConstraintName("Department_RoomOfHead_fkey");

                entity.HasMany<Room>(e => e.Rooms)
                    .WithOne(e => e.Department)
                    .HasForeignKey(e => e.DepartmentId)
                    .HasConstraintName("fk_department_to_rooms");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.ToTable("Employee");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.HasOne(d => d.EmployeeTitle)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.EmployeeTitleId)
                    .HasConstraintName("Employee_EmployeeTitleId_fkey");
            });

            modelBuilder.Entity<EmployeeTitle>(entity =>
            {
                entity.ToTable("EmployeeTitle");

                entity.Property(e => e.Id).UseIdentityAlwaysColumn();

                entity.Property(e => e.TitleName)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<EquipmentType>(entity =>
            {
                entity.ToTable("EquipmentType");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.TypeName).HasMaxLength(500);

                entity.HasMany(e => e.PropertiesForType)
                    .WithMany(e => e.EquipmentTypes);
            });

            modelBuilder.Entity<FloorInfo>(entity =>
            {
                entity.HasKey(e => new
                    {
                        e.Id,
                        e.FloorNumber
                    })
                    .HasName("FloorInfo_pkey");

                entity.ToTable("FloorInfo");

                entity.Property(e => e.ClassroomToImageMapping).HasColumnType("json");

                entity.Property(e => e.FloorImagePath)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.HasOne(d => d.BuildingNavigation)
                    .WithMany(p => p.FloorInfoes)
                    .HasForeignKey(d => d.Building)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FloorInfo_Building_fkey");
            });

            modelBuilder.Entity<Group>(entity =>
            {
                entity.ToTable("Group");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.GroupName)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasDefaultValueSql("'рога и олени'::character varying");

                entity.Property(e => e.NumberOfStudents).HasDefaultValueSql("1");

                entity.Property(e => e.SubGroupsNumber).HasDefaultValueSql("1");
            });

            modelBuilder.Entity<GroupLoad>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("GroupLoad");

                entity.HasOne(d => d.Employee)
                    .WithMany()
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("groupload_employee_id_fk");

                entity.HasOne(d => d.Group)
                    .WithMany()
                    .HasForeignKey(d => d.GroupId)
                    .HasConstraintName("groupload_group_id_fk");

                entity.HasOne(d => d.LessonType)
                    .WithMany()
                    .HasForeignKey(d => d.LessonTypeId)
                    .HasConstraintName("groupload_schedulerecordtype_id_fk");
            });

            modelBuilder.Entity<InternalRoomProperty>(entity =>
            {
                entity.HasComment("Some values regarding internal properties of room");

                entity.HasIndex(e => e.Ceiling, "internalroomproperties_ceiling_index");

                entity.HasIndex(e => e.Doors, "internalroomproperties_doors_index");

                entity.HasIndex(e => e.FloorMaterial, "internalroomproperties_floormaterial_index");

                entity.HasIndex(e => e.SanitaryEquipment, "internalroomproperties_sanitaryequipment_index");

                entity.HasIndex(e => e.Walls, "internalroomproperties_walls_index");

                entity.HasIndex(e => e.WindowsType, "internalroomproperties_windowstype_index");

                entity.Property(e => e.Ceiling).HasColumnType("character varying");

                entity.Property(e => e.Doors).HasColumnType("character varying");

                entity.Property(e => e.FloorMaterial).HasColumnType("character varying");

                entity.Property(e => e.LampsCount).HasDefaultValueSql("0");

                entity.Property(e => e.SanitaryEquipment).HasColumnType("character varying");

                entity.Property(e => e.Walls).HasColumnType("character varying");

                entity.Property(e => e.WindowsType).HasColumnType("character varying");

                entity.HasOne(d => d.Room)
                    .WithOne(p => p.InternalRoomProperties)
                    .HasConstraintName("internalroomproperties_room_id_fk")
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Room>(entity =>
            {
                entity.ToTable("Room");

                entity.HasComment("Room description");

                entity.HasIndex(e => e.BuildingId, "fki_BuildingFK");

                entity.HasIndex(e => e.RoomTypeId, "fki_RoomTypeFK");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Area)
                    .HasColumnName("area")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.LastMaintenance).HasComment("Date of last maintenance");

                entity.Property(e => e.RoomNumber)
                    .IsRequired()
                    .HasMaxLength(5);

                entity.Property(e => e.Seats)
                    .HasColumnName("seats")
                    .HasDefaultValueSql("'-1'::integer");

                entity.HasOne(d => d.FloorInfo)
                    .WithMany(p => p.Rooms)
                    .HasForeignKey("FloorId", "RoomFloor")
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_FloorInfo_Room");

                entity.HasOne(d => d.Building)
                    .WithMany(p => p.Rooms)
                    .HasForeignKey(d => d.BuildingId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("BuildingFK");

                entity.HasOne(d => d.RoomTarget)
                    .WithMany(p => p.Rooms)
                    .HasForeignKey(d => d.RoomTargetId)
                    .HasConstraintName("room_roomtarget_id_fk");

                entity.HasOne(d => d.RoomType)
                    .WithMany(p => p.Rooms)
                    .HasForeignKey(d => d.RoomTypeId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("RoomTypeFK");
            });

            modelBuilder.Entity<RoomTarget>(entity =>
            {
                entity.ToTable("RoomTarget");

                entity.HasComment("Room targets");

                entity.HasIndex(e => e.TargetName, "roomtarget_targetname_index");

                entity.Property(e => e.TargetName)
                    .IsRequired()
                    .HasColumnType("character varying")
                    .HasDefaultValueSql("'Неизвестно'::character varying");
            });

            modelBuilder.Entity<RoomType>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<Schedule>(entity =>
            {
                entity.ToTable("Schedule");

                entity.HasIndex(e => e.RoomId, "fk_schedule_room");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ForSubGroup).HasDefaultValueSql("'-1'::integer");

                entity.Property(e => e.WeekType).HasDefaultValueSql("0");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.Schedules)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Schedule_GroupId_fkey");

                entity.HasOne(d => d.Room)
                    .WithMany(p => p.Schedules)
                    .HasForeignKey(d => d.RoomId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("Schedule_RoomId_fkey");

                entity.HasOne(d => d.ScheduleTime)
                    .WithMany(p => p.Schedules)
                    .HasForeignKey(d => d.ScheduleTimeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Schedule_ScheduleTimeId_fkey");

                entity.HasOne(d => d.Tutor)
                    .WithMany(p => p.Schedules)
                    .HasForeignKey(d => d.TutorId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("Schedule_TutorId_fkey");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.Schedules)
                    .HasForeignKey(d => d.TypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Schedule_TypeId_fkey");
            });

            modelBuilder.Entity<ScheduleRecordType>(entity =>
            {
                entity.ToTable("ScheduleRecordType");

                entity.Property(e => e.Id).UseIdentityAlwaysColumn();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<ScheduleTime>(entity =>
            {
                entity.ToTable("ScheduleTime");

                entity.Property(e => e.Id).UseIdentityAlwaysColumn();

                entity.Property(e => e.LessonEnd).HasColumnType("time with time zone");

                entity.Property(e => e.LessonStart).HasColumnType("time with time zone");

                entity.HasOne(d => d.Building)
                    .WithMany(p => p.ScheduleTimes)
                    .HasForeignKey(d => d.BuildingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ScheduleTime_BuildingId_fkey");
            });

            modelBuilder.Entity<Ticket>(entity =>
            {
                entity.ToTable("Ticket");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.HasOne(d => d.Assignee)
                    .WithMany(p => p.TicketAssignees)
                    .HasForeignKey(d => d.AssigneeId)
                    .HasConstraintName("Ticket_AssigneeId_fkey");

                entity.HasOne(d => d.ReporterNavigation)
                    .WithMany(p => p.TicketReporterNavigations)
                    .HasForeignKey(d => d.Reporter)
                    .HasConstraintName("Ticket_Reporter_fkey");
            });

            modelBuilder.Entity<TicketType>(entity =>
            {
                entity.ToTable("TicketType");

                entity.Property(e => e.Id).UseIdentityAlwaysColumn();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.HasOne(d => d.NotifyFirstNavigation)
                    .WithMany(p => p.TicketTypes)
                    .HasForeignKey(d => d.NotifyFirst)
                    .HasConstraintName("TicketType_NotifyFirst_fkey");
            });
        }
    }
}