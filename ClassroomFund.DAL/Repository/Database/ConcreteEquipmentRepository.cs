﻿using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;

namespace ClassroomFund.DAL.Repository.Database;

public class ConcreteEquipmentRepository : IConcreteEquipmentRepository
{
    private readonly ClassroomFundContext _context;
    private readonly DbSet<ConcreteEquipment> _concreteEquipment;

    public ConcreteEquipmentRepository(ClassroomFundContext context)
    {
        _context = context;
        _concreteEquipment = _context.ConcreteEquipments;
    }

    public async Task<List<ConcreteEquipment>> GetAll()
    {
        return await _concreteEquipment.ToListAsync();
    }

    public async Task<List<ConcreteEquipment>> GetAllWithJoin()
    {
        return await _concreteEquipment
            .Include(e => e.Properies)
            .Include(e => e.Room)
            .Include(e => e.Type)
            .ToListAsync();
    }

    public async Task<ConcreteEquipment> GetById(Guid id)
    {
        return await _concreteEquipment.FindAsync(id);
    }

    public async Task<ConcreteEquipment> GetByIdWithJoin(Guid id)
    {
        return await _concreteEquipment
            .Include(e => e.Properies)
            .Include(e => e.Room)
            .Include(e => e.Type)
            .FirstOrDefaultAsync(e => e.Id == id);
    }

    public async Task<ConcreteEquipment> Add(ConcreteEquipment entity)
    {
        if (entity.Id == Guid.Empty)
        {
            entity.Id = Guid.NewGuid();
        }

        var result = await _concreteEquipment.AddAsync(entity);
        await _context.SaveChangesAsync();
        return result.Entity;
    }

    public async Task<ConcreteEquipment> Update(ConcreteEquipment entity)
    {
        var updated = _concreteEquipment.Update(entity);
        await _context.SaveChangesAsync();
        return updated.Entity;
    }

    public async Task Delete(Guid id)
    {
        var entity = await _concreteEquipment.FindAsync(id);
        if (entity != null)
        {
            _concreteEquipment.Remove(entity);
            await _context.SaveChangesAsync();
        }
    }
}