﻿using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;

namespace ClassroomFund.DAL.Repository.Database;

public class RoomInformationRepository : IRoomInformationRepository
{
    private readonly ClassroomFundContext _context;

    public RoomInformationRepository(ClassroomFundContext context)
    {
        _context = context;
    }

    public async Task<List<InternalRoomProperty>> GetAll()
    {
        return await _context.InternalRoomProperties.ToListAsync();
    }

    public async Task<List<InternalRoomProperty>> GetAllWithJoin()
    {
        return await _context.InternalRoomProperties
            .Include(e=>e.Room)
            .ToListAsync();
    }

    public async Task<InternalRoomProperty> GetById(int id)
    {
        return await _context.InternalRoomProperties.FirstOrDefaultAsync(e=>e.Id == id);
    }

    public async Task<InternalRoomProperty> GetByIdWithJoin(int id)
    {
        return await _context.InternalRoomProperties
            .Include(e=>e.Room)
            .FirstOrDefaultAsync(e=>e.Id == id);
    }

    public async Task<InternalRoomProperty> Add(InternalRoomProperty entity)
    {
        if (entity.Id != 0)
        {
            entity.Id = 0;
        }
        var added = await _context.InternalRoomProperties.AddAsync(entity);
        await _context.SaveChangesAsync();
        return added.Entity;
    }

    public async Task<InternalRoomProperty> Update(InternalRoomProperty entity)
    {
        if(entity == null)
        {
            throw new ArgumentNullException(nameof(entity));
        }
        var updated = _context.InternalRoomProperties.Update(entity);
        await _context.SaveChangesAsync();
        return updated.Entity;
    }

    public async Task Delete(int id)
    {
        var entity = await _context.InternalRoomProperties.FindAsync(id);
        if (entity is not null)
        {
            _context.InternalRoomProperties.Remove(entity);
        }
        await _context.SaveChangesAsync();
    }

    public async Task<InternalRoomProperty> GetByRoomId(Guid id)
    {
        return await _context.InternalRoomProperties.FirstOrDefaultAsync(e => e.RoomId == id);
    }
}
