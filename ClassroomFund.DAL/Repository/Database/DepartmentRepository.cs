﻿using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;

namespace ClassroomFund.DAL.Repository.Database;

public class DepartmentRepository : IDepartmentRepository
{
    private readonly ClassroomFundContext _context;

    public DepartmentRepository(ClassroomFundContext context)
    {
        _context = context;
    }

    public async Task<List<Department>> GetAll()
    {
        return await _context.Departments.ToListAsync();
    }

    public async Task<List<Department>> GetAllWithJoin()
    {
        return await _context.Departments
            .Include(e=>e.Buildings)
            .Include(e=>e.HeadOfDepartment)
            .Include(e=>e.RoomOfHeadNavigation)
            .ToListAsync();
    }

    public async Task<Department> GetById(int id)
    {
        return await _context.Departments.FirstOrDefaultAsync(e=>e.Id == id);
    }

    public async Task<Department> GetByIdWithJoin(int id)
    {
        return (await _context.Departments
            .Include(e => e.Buildings)
            .Include(e => e.HeadOfDepartment)
            .Include(e => e.RoomOfHeadNavigation)
            .Include(e=>e.Rooms)
            .FirstOrDefaultAsync(e => e.Id == id));
    }

    public async Task<Department> Add(Department entity)
    {
        if (entity is not {Id: 0})
        {
            entity.Id = 0;
        }
        var added = await _context.Departments.AddAsync(entity);
        await _context.SaveChangesAsync();
        return added.Entity;
    }

    public async Task<Department> Update(Department entity)
    {
        var updated = _context.Departments.Update(entity);
        await _context.SaveChangesAsync();
        return updated.Entity;
    }

    public async Task Delete(int id)
    {
        var entity = await _context.Departments.FindAsync(id);
        if (entity is not null)
        {
            _context.Departments.Remove(entity);
        }
        await _context.SaveChangesAsync();
    }
}
