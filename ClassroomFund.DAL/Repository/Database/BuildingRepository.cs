﻿global using Microsoft.EntityFrameworkCore;
using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;

namespace ClassroomFund.DAL.Repository.Database
{
    public class BuildingRepository : IBuildingRepository
    {
        private readonly ClassroomFundContext _context;

        public BuildingRepository(ClassroomFundContext context)
        {
            _context = context;
        }
        public async Task<List<Building>> GetAll()
        {
            return await _context.Buildings.OrderBy(e => e.Name).ToListAsync();
        }

        public async Task<List<Building>> GetAllWithJoin()
        {
            return await _context.Buildings
                .OrderBy(e=>e.Name)
                .Include(e => e.FloorInfoes)
                .Include(e => e.Rooms)
                .Include(e => e.Departments)
                .ToListAsync();
        }

        public async Task<Building> GetById(Guid id)
        {
            return await _context.Buildings
                .OrderBy(e => e.Name)   
                .Where(e => e.Id.Equals(id))
                .FirstOrDefaultAsync();
        }

        public async Task<Building> GetByIdWithJoin(Guid id)
        {
            return await _context.Buildings
                .OrderBy(e => e.Name)
                .Where(e => e.Id.Equals(id))
                .Include(e => e.FloorInfoes)
                .Include(e => e.Rooms)
                .Include(e => e.Departments).FirstOrDefaultAsync();
        }

        public async Task<Building> Add(Building entity)
        {
            if (entity?.Id == null)
            {
                entity.Id = Guid.NewGuid();
            }
            var added =  await _context.Buildings.AddAsync(entity);
            await _context.SaveChangesAsync();
            return added.Entity;
        }

        public async Task<Building> Update(Building entity)
        {
            var updated = _context.Buildings.Update(entity);
            await _context.SaveChangesAsync();
            return updated.Entity;
        }

        public async Task Delete(Guid id)
        {
            var entity = await _context.Buildings.FindAsync(id);
            if (entity is not null)
            {
                _context.Buildings.Remove(entity);
            }
            await _context.SaveChangesAsync();
        }
    }
}
