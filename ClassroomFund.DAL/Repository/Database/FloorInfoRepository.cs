﻿using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassroomFund.DAL.Repository.Database
{
    public class FloorInfoRepository : IFloorInfoRepository
    {
        private readonly ClassroomFundContext _context;

        public FloorInfoRepository(ClassroomFundContext context)
        {
            _context = context;
        }

        public async Task<List<FloorInfo>> GetAll()
        {
            return await _context.FloorInfoes.OrderBy(e => e.FloorNumber).ToListAsync();
        }

        public async Task<List<FloorInfo>> GetAllWithJoin()
        {
            return await _context.FloorInfoes
                .OrderBy(e => e.FloorNumber)
                .Include(e => e.BuildingNavigation)
                .ToListAsync();
        }

        public async Task<FloorInfo> GetById(Guid id)
        {
            return await _context.FloorInfoes
                .OrderBy(e => e.FloorNumber)
                .FirstOrDefaultAsync(e => e.Id.Equals(id));
        }

        public async Task<FloorInfo> GetByIdWithJoin(Guid id)
        {
            return await _context.FloorInfoes
                .OrderBy(e => e.FloorNumber)
                .Include(e => e.BuildingNavigation)
                .FirstOrDefaultAsync(e => e.Id.Equals(id));
        }

        public async Task<FloorInfo> Add(FloorInfo entity)
        {
            if (entity?.Id == null)
            {
                entity.Id = Guid.NewGuid();
            }
            var added = await _context.FloorInfoes.AddAsync(entity);
            await _context.SaveChangesAsync();
            return added.Entity;
        }

        public async Task<FloorInfo> Update(FloorInfo entity)
        {
            var updated = _context.FloorInfoes.Update(entity);
            await _context.SaveChangesAsync();
            return updated.Entity;
        }

        public async Task Delete(Guid id)
        {
            var entity = await _context.FloorInfoes.FindAsync(id);
            if (entity is not null)
            {
                _context.FloorInfoes.Remove(entity);
            }
            await _context.SaveChangesAsync();
        }

        public async Task<List<FloorInfo>> GetAllFloorsByBuildingId(Guid id)
        {
            return await _context.FloorInfoes
                .Where(e => e.Building == id)
                .OrderBy(e => e.FloorNumber)
                .ToListAsync();
        }
    }
}
