﻿using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;

namespace ClassroomFund.DAL.Repository.Database;

public class RoomTypeRepository : IRoomTypeRepository
{
    private readonly ClassroomFundContext _context;

    public RoomTypeRepository(ClassroomFundContext context)
    {
        _context = context;
    }

    public async Task<List<RoomType>> GetAll()
    {
        return await _context.RoomTypes.ToListAsync();
    }

    public async Task<List<RoomType>> GetAllWithJoin()
    {
        return await _context.RoomTypes
            .Include(e=>e.Rooms)
            .ToListAsync();
    }

    public async Task<RoomType> GetById(Guid id)
    {
        return await _context.RoomTypes.FirstOrDefaultAsync(e=>e.Id == id);
    }

    public async Task<RoomType> GetByIdWithJoin(Guid id)
    {
        return await _context.RoomTypes
            .Include(e=>e.Rooms)
            .FirstOrDefaultAsync(e=>e.Id == id);
    }

    public async Task<RoomType> Add(RoomType entity)
    {
        if (entity.Id == Guid.Empty)
        {
            entity.Id = Guid.NewGuid();
        }
        var added = await _context.RoomTypes.AddAsync(entity);
        await _context.SaveChangesAsync();
        return added.Entity;
    }

    public async Task<RoomType> Update(RoomType entity)
    {
        var updated = _context.RoomTypes.Update(entity);
        await _context.SaveChangesAsync();
        return updated.Entity;
    }

    public async Task Delete(Guid id)
    {
        var entity = await _context.RoomTypes.FindAsync(id);
        if (entity is not null)
        {
            _context.RoomTypes.Remove(entity);
        }
        await _context.SaveChangesAsync();

    }
}
