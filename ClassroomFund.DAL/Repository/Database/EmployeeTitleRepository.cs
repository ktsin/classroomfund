﻿using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;

namespace ClassroomFund.DAL.Repository.Database;

public class EmployeeTitleRepository : IEmployeeTitleRepository
{
    private readonly ClassroomFundContext _context;

    public EmployeeTitleRepository(ClassroomFundContext context)
    {
        _context = context;
    }

    public async Task<List<EmployeeTitle>> GetAll()
    {
        return await _context.EmployeeTitles.ToListAsync();
    }

    public async Task<List<EmployeeTitle>> GetAllWithJoin()
    {
        return await _context.EmployeeTitles
            .Include(e=>e.Employees)
            .ToListAsync();
    }

    public async Task<EmployeeTitle> GetById(int id)
    {
        return await _context.EmployeeTitles
            .FirstOrDefaultAsync(e=>e.Id.Equals(id));
    }

    public async Task<EmployeeTitle> GetByIdWithJoin(int id)
    {
        return await _context.EmployeeTitles
            .Include(e=>e.Employees)
            .FirstOrDefaultAsync(e=>e.Id.Equals(id));
    }

    public async Task<EmployeeTitle> Add(EmployeeTitle entity)
    {
        if (entity is not {Id: 0})
        {
            entity.Id = 0;
        }
        var added = await _context.EmployeeTitles.AddAsync(entity);
        await _context.SaveChangesAsync();
        return added.Entity;
    }

    public async Task<EmployeeTitle> Update(EmployeeTitle entity)
    {
        var updated = _context.EmployeeTitles.Update(entity);
        await _context.SaveChangesAsync();
        return updated.Entity;
    }

    public async Task Delete(int id)
    {
        var entity = await _context.RoomTypes.FindAsync(id);
        if (entity is not null)
        {
            _context.RoomTypes.Remove(entity);
        }
        await _context.SaveChangesAsync();
    }
}
