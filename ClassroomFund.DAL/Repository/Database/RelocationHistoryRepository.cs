﻿using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;

namespace ClassroomFund.DAL.Repository.Database;

public class RelocationHistoryRepository : IRelocationHistoryRepository
{
    private readonly ClassroomFundContext _context;
    private readonly DbSet<ClassroomMovementHistory> _movementHistory;

    public RelocationHistoryRepository(ClassroomFundContext context)
    {
        _context = context;
        _movementHistory = context.ClassroomMovementHistories;
    }

    public async Task<List<ClassroomMovementHistory>> GetAll()
    {
        return await _movementHistory.ToListAsync();
    }

    public async Task<List<ClassroomMovementHistory>> GetAllWithJoin()
    {
        return await _movementHistory
            .Include(e => e.FromDepartmentNavigation)
            .Include(e => e.ToDepartmentNavigation)
            .Include(e => e.Classroom)
            .ToListAsync();
    }

    public async Task<ClassroomMovementHistory> GetById(Guid id)
    {
        return await _movementHistory.FirstOrDefaultAsync(e => e.Id == id);
    }

    public async Task<ClassroomMovementHistory> GetByIdWithJoin(Guid id)
    {
        return await _movementHistory
            .Include(e => e.FromDepartmentNavigation)
            .Include(e => e.ToDepartmentNavigation)
            .Include(e => e.Classroom)
            .FirstOrDefaultAsync(e => e.Id == id);
    }

    public async Task<ClassroomMovementHistory> Add(ClassroomMovementHistory entity)
    {
        if (entity.Id == Guid.Empty)
        {
            entity.Id = Guid.NewGuid();
        }

        var result = await _movementHistory.AddAsync(entity);
        await _context.SaveChangesAsync();
        return result.Entity;
    }

    public async Task<ClassroomMovementHistory> Update(ClassroomMovementHistory entity)
    {
        var result = _movementHistory.Update(entity);
        await _context.SaveChangesAsync();
        return result.Entity;
    }

    public async Task Delete(Guid id)
    {
        var entity = await GetById(id);
        if (entity == null)
        {
            return;
        }
        _movementHistory.Remove(entity);
        await _context.SaveChangesAsync();
    }
}