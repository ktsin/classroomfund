﻿using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;

namespace ClassroomFund.DAL.Repository.Database;

public class EmployeeRepository : IEmployeeRepository
{
    private readonly ClassroomFundContext _context;

    public EmployeeRepository(ClassroomFundContext context)
    {
        _context = context;
    }

    public async Task<List<Employee>> GetAll()
    {
        return await _context.Employees.ToListAsync();
    }

    public async Task<List<Employee>> GetAllWithJoin()
    {
        return await _context.Employees
            .Include(e => e.EmployeeTitle)
            .Include(e => e.TicketTypes)
            .Include(e=>e.Department)
            .Include(e=>e.MyDepartment)
            .ToListAsync();
    }

    public async Task<Employee> GetById(Guid id)
    {
        return await _context.Employees.FirstOrDefaultAsync(e => e.Id.Equals(id));
    }

    public async Task<Employee> GetByIdWithJoin(Guid id)
    {
        return await _context.Employees
            .Include(e => e.EmployeeTitle)
            .Include(e => e.TicketTypes)
            .Include(e=>e.TicketAssignees)
            .Include(e=>e.Department)
            .Include(e=>e.MyDepartment)
            .FirstOrDefaultAsync(e => e.Id.Equals(id));
    }

    public async Task<Employee> Add(Employee entity)
    {
        if (entity?.Id == Guid.Empty)
        {
            entity.Id = Guid.NewGuid();
        }
        var added = await _context.Employees.AddAsync(entity);
        await _context.SaveChangesAsync();
        return added.Entity;
    }

    public async Task<Employee> Update(Employee entity)
    {
        var updated = _context.Employees.Update(entity);
        await _context.SaveChangesAsync();
        return updated.Entity;
    }

    public async Task Delete(Guid id)
    {
        var entity = await _context.Employees.FindAsync(id);
        if (entity != null)
        {
            _context.Employees.Remove(entity);
            await _context.SaveChangesAsync();
        }
    }
}
