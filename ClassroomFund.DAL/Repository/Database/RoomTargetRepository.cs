﻿using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;

namespace ClassroomFund.DAL.Repository.Database;

public class RoomTargetRepository : IRoomTargetRepository
{
    private readonly ClassroomFundContext _context;

    public RoomTargetRepository(ClassroomFundContext context)
    {
        _context = context;
    }

    public async Task<List<RoomTarget>> GetAll()
    {
        return await _context.RoomTargets.ToListAsync();
    }

    public async Task<List<RoomTarget>> GetAllWithJoin()
    {
        return await _context.RoomTargets
            .Include(e=>e.Rooms)
            .ToListAsync();
    }

    public async Task<RoomTarget> GetById(int id)
    {
        return await _context.RoomTargets.FirstOrDefaultAsync(e=>e.Id == id);
    }

    public async Task<RoomTarget> GetByIdWithJoin(int id)
    {
        return await _context.RoomTargets
            .Include(e=>e.Rooms)
            .FirstOrDefaultAsync(e=>e.Id == id);
    }

    public async Task<RoomTarget> Add(RoomTarget entity)
    {
        if (entity.Id != 0)
        {
            entity.Id = 0;
        }
        var added = await _context.RoomTargets.AddAsync(entity);
        await _context.SaveChangesAsync();
        return added.Entity;
    }

    public async Task<RoomTarget> Update(RoomTarget entity)
    {
        var updated = _context.RoomTargets.Update(entity);
        await _context.SaveChangesAsync();
        return updated.Entity;
    }

    public async Task Delete(int id)
    {
        var entity = await _context.RoomTargets.FindAsync(id);
        if (entity is not null)
        {
            _context.RoomTargets.Remove(entity);
        }
        await _context.SaveChangesAsync();
    }
}
