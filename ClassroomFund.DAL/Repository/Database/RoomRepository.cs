﻿using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;

namespace ClassroomFund.DAL.Repository.Database;

public class RoomRepository : IRoomRepository
{
    private readonly ClassroomFundContext _context;

    public RoomRepository(ClassroomFundContext context)
    {
        _context = context;
    }

    public async Task<List<Room>> GetAll()
    {
        return await _context.Rooms
            .Include(e => e.RoomTarget)
            .Include(e => e.RoomType)
            .ToListAsync();
    }

    public async Task<List<Room>> GetAllWithJoin()
    {
        return await _context.Rooms
            .Include(e => e.Building)
            .Include(e => e.Department)
            .Include(e => e.ConcreteEquipments)
            .ThenInclude(e=>e.Properies)
            .ThenInclude(e=>e.EquipmentProperty)
            .Include(e => e.ConcreteEquipments)
            .ThenInclude(e=>e.Type)
            .Include(e => e.ClassroomMovementHistories)
            .Include(e => e.InternalRoomProperties)
            .Include(e => e.RoomTarget)
            .Include(e => e.RoomType)
            .ToListAsync();
    }

    public async Task<Room> GetById(Guid id)
    {
        return await _context.Rooms
            .Include(e => e.RoomTarget)
            .Include(e => e.RoomType)
            .FirstOrDefaultAsync(e => e.Id == id);
    }

    public async Task<Room> GetByIdWithJoin(Guid id)
    {
        return await _context.Rooms
            .Where(e => e.Id == (Guid)id)
            .Include(e => e.Building)
            .Include(e => e.Department)
            .Include(e => e.ConcreteEquipments)
            .Include(e => e.ClassroomMovementHistories)
            .Include(e => e.InternalRoomProperties)
            .Include(e => e.RoomTarget)
            .Include(e => e.RoomType)
            .FirstOrDefaultAsync();
    }

    public async Task<Room> Add(Room entity)
    {
        if (entity?.Id == null)
        {
            entity.Id = Guid.NewGuid();
        }
        var added = await _context.Rooms.AddAsync(entity);
        await _context.SaveChangesAsync();
        return added.Entity;
    }

    public async Task<Room> Update(Room entity)
    {
        var updated = _context.Rooms.Update(entity);
        await _context.SaveChangesAsync();
        return updated.Entity;
    }

    public async Task Delete(Guid id)
    {
        var entity = await _context.Rooms.FindAsync(id);
        if (entity != null)
        {
            _context.Remove(entity);
            await _context.SaveChangesAsync();
        }
    }

    public async Task<List<Room>> GetRoomsByFloorId(Guid floorId)
    {
        return await _context.Rooms
            .Where(e => e.FloorId == floorId)
            .Include(e => e.RoomTarget)
            .Include(e => e.RoomType)
            .ToListAsync();
    }

    public async Task<List<Room>> GetRoomsByBuildingId(Guid buildingId)
    {
        return await _context.Rooms
            .Where(e => e.BuildingId == buildingId)
            .Include(e => e.RoomTarget)
            .Include(e => e.RoomType)
            .ToListAsync();
    }

    public async Task<List<Room>> GetRoomsByDepartmentId(int departmentId)
    {
        return await _context.Rooms
            .Include(e => e.Department)
            .Where(e => e.Department != null && e.Department.Id == departmentId)
            .Include(e => e.RoomTarget)
            .Include(e => e.RoomType)
            .ToListAsync();
    }

    public async Task<List<Room>> GetRoomsByFloorIdWithJoin(Guid floorId)
    {
        return await _context.Rooms
            .Where(e => e.FloorId == floorId)
            .Include(e => e.Building)
            .Include(e => e.Department)
            .Include(e => e.ConcreteEquipments)
            .Include(e => e.ClassroomMovementHistories)
            .Include(e => e.InternalRoomProperties)
            .Include(e => e.RoomTarget)
            .Include(e => e.RoomType)
            .ToListAsync();
    }

    public async Task<List<Room>> GetRoomsByBuildingIdWithJoin(Guid buildingId)
    {
        return await _context.Rooms
            .Where(e => e.BuildingId == buildingId)
            .Include(e => e.Building)
            .Include(e => e.Department)
            .Include(e => e.ConcreteEquipments)
            .Include(e => e.ClassroomMovementHistories)
            .Include(e => e.InternalRoomProperties)
            .Include(e => e.RoomTarget)
            .Include(e => e.RoomType)
            .ToListAsync();
    }

    public async Task<List<Room>> GetRoomsByDepartmentIdWithJoin(int departmentId)
    {
        return await _context.Rooms
            .Include(e => e.Department)
            .Where(e => e.Department != null && e.Department.Id == departmentId)
            .Include(e => e.Building)
            .Include(e => e.ConcreteEquipments)
            .Include(e => e.ClassroomMovementHistories)
            .Include(e => e.InternalRoomProperties)
            .Include(e => e.RoomTarget)
            .Include(e => e.RoomType)
            .ToListAsync();
    }
}
