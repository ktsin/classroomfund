﻿using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;

namespace ClassroomFund.DAL.Repository.Database;

public class EquipmentPropertyRepository : IEquipmentPropertyRepository
{
    private readonly ClassroomFundContext _context;
    
    public EquipmentPropertyRepository(ClassroomFundContext context)
    {
        _context = context;
    }
    
    public async Task<List<EquipmentProperty>> GetAll()
    {
        return await _context.EquipmentProperties.ToListAsync();
    }

    public async Task<List<EquipmentProperty>> GetAllWithJoin()
    {
        return await _context.EquipmentProperties
            .Include(x => x.EquipmentTypes)
            .ToListAsync();
    }

    public async Task<EquipmentProperty> GetById(Guid id)
    {
        return await _context.EquipmentProperties.FindAsync(id);
    }

    public async Task<EquipmentProperty> GetByIdWithJoin(Guid id)
    {
        return await _context.EquipmentProperties
            .Include(x => x.EquipmentTypes)
            .FirstOrDefaultAsync(x => x.Id == id);
    }

    public async Task<EquipmentProperty> Add(EquipmentProperty entity)
    {
        if (entity.Id == Guid.Empty)
        {
            entity.Id = Guid.NewGuid();
        }
        var added = await _context.EquipmentProperties.AddAsync(entity);
        await _context.SaveChangesAsync();
        return added.Entity;
    }

    public async Task<EquipmentProperty> Update(EquipmentProperty entity)
    {
        var updated = _context.EquipmentProperties.Update(entity);
        await _context.SaveChangesAsync();
        return updated.Entity;
    }

    public async Task Delete(Guid id)
    {
        var entity = await _context.EquipmentProperties.FindAsync(id);
        if(entity != null)
        {
            _context.EquipmentProperties.Remove(entity);
            await _context.SaveChangesAsync();
        }
    }
}
