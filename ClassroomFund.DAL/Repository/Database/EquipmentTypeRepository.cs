﻿using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;

namespace ClassroomFund.DAL.Repository.Database;

public class EquipmentTypeRepository : IEquipmentTypeRepository
{
    private readonly ClassroomFundContext _context;

    public EquipmentTypeRepository(ClassroomFundContext context)
    {
        _context = context;
    }

    public async Task<List<EquipmentType>> GetAll()
    {
        return await _context.EquipmentTypes.ToListAsync();
    }

    public async Task<List<EquipmentType>> GetAllWithJoin()
    {
        return await _context.EquipmentTypes
            .Include(e=>e.PropertiesForType)
            .Include(e=>e.ConcreteEquipments)
            .ToListAsync();
    }

    public async Task<EquipmentType> GetById(Guid id)
    {
        return await _context.EquipmentTypes.FindAsync(id);
    }

    public async Task<EquipmentType> GetByIdWithJoin(Guid id)
    {
        return await _context.EquipmentTypes
            .Include(e=>e.PropertiesForType)
            .Include(e=>e.ConcreteEquipments)
            .FirstOrDefaultAsync(e=>e.Id == id);
    }

    public async Task<EquipmentType> Add(EquipmentType entity)
    {
        if (entity == null)
            throw new ArgumentNullException(nameof(entity));
        if(entity.Id == Guid.Empty)
            entity.Id = Guid.NewGuid();
        var added = await _context.AddAsync(entity);
        await _context.SaveChangesAsync();
        return added.Entity;
    }

    public async Task<EquipmentType> Update(EquipmentType entity)
    {
        if (entity == null)
            throw new ArgumentNullException(nameof(entity));
        var updated = _context.EquipmentTypes.Update(entity);
        await _context.SaveChangesAsync();
        return updated.Entity;
    }

    public async Task Delete(Guid id)
    {
        var entity = await _context.EquipmentTypes.FindAsync(id);
        if (entity is not null)
        {
            _context.EquipmentTypes.Remove(entity);
        }
        await _context.SaveChangesAsync();
    }
}
