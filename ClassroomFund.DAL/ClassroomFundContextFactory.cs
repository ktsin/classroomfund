﻿using ClassroomFund.DAL.Repository.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace ClassroomFund.ScheduleUI;

public class ClassroomFundContextFactory : IDesignTimeDbContextFactory<ClassroomFundContext>
{
    public ClassroomFundContext CreateDbContext(string[] args)
    {
        var optionsBuilder = new DbContextOptionsBuilder<ClassroomFundContext>();
        optionsBuilder.UseNpgsql("Server=127.0.0.1;Port=5432;Database=classroomFund;User Id=postgres;Password=8xFrgDmb;");

        return new ClassroomFundContext(optionsBuilder.Options);
    }
}