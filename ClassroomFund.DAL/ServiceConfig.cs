﻿using ClassroomFund.DAL.Repository.Database;
using ClassroomFund.DAL.Repository.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ClassroomFund.DAL
{
    public static class ServiceConfig
    {
        public static IServiceCollection ConfigureDataLayer(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContextPool<ClassroomFundContext>(e =>
                {
                e.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
                e.UseNpgsql(configuration.GetConnectionString("MainConnection"),
                builder => builder.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery));
                // e.UseLoggerFactory(NullLoggerFactory.Instance);
                e.EnableDetailedErrors();
                },
            2048);

            #region Repositories

            services.AddTransient<IBuildingRepository, BuildingRepository>();
            services.AddTransient<IFloorInfoRepository, FloorInfoRepository>();
            services.AddTransient<IRoomRepository, RoomRepository>();
            services.AddTransient<IEmployeeRepository, EmployeeRepository>();
            services.AddTransient<IEmployeeTitleRepository, EmployeeTitleRepository>();
            services.AddTransient<IDepartmentRepository, DepartmentRepository>();
            services.AddTransient<IRoomTypeRepository, RoomTypeRepository>();
            services.AddTransient<IRoomTargetRepository, RoomTargetRepository>();
            services.AddTransient<IRoomInformationRepository, RoomInformationRepository>();
            services.AddTransient<IEquipmentPropertyRepository, EquipmentPropertyRepository>();
            services.AddTransient<IEquipmentTypeRepository, EquipmentTypeRepository>();
            services.AddTransient<IRelocationHistoryRepository, RelocationHistoryRepository>();
            services.AddTransient<IConcreteEquipmentRepository, ConcreteEquipmentRepository>();
            
            #endregion


            return services;
        }
    }
}
