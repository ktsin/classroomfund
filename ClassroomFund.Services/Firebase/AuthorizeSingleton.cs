﻿using System.Net;
using System.Text;
using Newtonsoft.Json.Linq;

namespace ClassroomFund.Services.Firebase;

public class AuthorizeSingleton
{
    private static readonly object Locker = new object();
    private static AuthorizeSingleton _instance = null;

    private AuthorizeSingleton()
    {
        Initialization = Initialize();
    }

    private async Task Initialize()
    {
        string webApiKey = Environment.GetEnvironmentVariable("FB_API_KEY");
        string pwd = Environment.GetEnvironmentVariable("FB_USER_PWD");
        string email = "test@example.com";
        using HttpClient client = new HttpClient();
        HttpRequestMessage msg = new HttpRequestMessage(HttpMethod.Post,
        $"https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key={webApiKey}");
        msg.Content = new StringContent($"{{\"email\":\"{email}\",\"password\":\"{pwd}\",\"returnSecureToken\":true}}",
        Encoding.UTF8,
        "application/json");
        var response = await client.SendAsync(msg);
        if (response.StatusCode != HttpStatusCode.OK)
        {
            throw new ApplicationException("Cannot retrieve token from Firebase!");
        }
        var parsedResponse = JObject.Parse(await response.Content.ReadAsStringAsync());
        Token = parsedResponse["idToken"]?.Value<string>() ?? null;
    }

    public static AuthorizeSingleton Instance
    {
        get
        {
            lock (Locker)
            {
                if (_instance == null || TokenIssued <= DateTime.Now)
                {
                    _instance = new AuthorizeSingleton();
                }
            }
            return _instance;
        }
    }

    public string Token { get; private set; }

    public Task Initialization { get; }
    
    private static DateTime TokenIssued { get; set; }
}

public interface IAsyncInitialization
{
    Task Initialization { get; }
}
