﻿namespace ClassroomFund.Services.Firebase;

public static class FirebaseUtilities
{
    public static async Task<string> GetBearerToken()
    {
        var authorize = AuthorizeSingleton.Instance;
        await authorize.Initialization;
        return authorize.Token;
    }
}
