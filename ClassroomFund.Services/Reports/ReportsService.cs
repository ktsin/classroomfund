﻿using FastReport.Export.PdfSimple;
using FastReport;
using FastReport.Export.Html;

namespace ClassroomFund.Services.Reports;

public class ReportsService
{
    public async Task<byte[]>  EquipmentInDepartment(int departmentId)
    {
        using MemoryStream stream = new MemoryStream();
        using (Report report = new Report())
        {
            report.Load(Path.Combine("Content\\ListOfEquipment.frx"));
            report.SetParameterValue("departmentIdParam", departmentId);
            report.Prepare();
            PDFSimpleExport pdf = new PDFSimpleExport();
            report.Export(pdf, stream);
        }
        return stream.ToArray();
    }
}