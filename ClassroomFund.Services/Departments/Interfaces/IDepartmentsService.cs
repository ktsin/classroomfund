﻿using ClassroomFund.Models;

namespace ClassroomFund.Services.Departments;

public interface IDepartmentsService
{
    Task<List<Department>> GetAll();
    Task<List<Department>> GetAllWithInfo();
    Task<Department> GetById(int id);
    Task<Department> GetByIdWithInfo(int id);
    Task<Department> Add(Department entity);
    Task RemoveDepartment(int id);
    Task<Department> UpdateDepartment(Department target);
}
