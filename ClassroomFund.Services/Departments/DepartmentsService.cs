﻿using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;

namespace ClassroomFund.Services.Departments;

public class DepartmentsService : IDepartmentsService
{
    private readonly IDepartmentRepository _departmentRepository;

    public DepartmentsService(IDepartmentRepository departmentRepository)
    {
        _departmentRepository = departmentRepository;
    }

    public async Task<List<Department>> GetAll()
    {
        return await _departmentRepository.GetAll();
    }

    public async Task<List<Department>> GetAllWithInfo()
    {
        return await _departmentRepository.GetAllWithJoin();
    }

    public async Task<Department> GetById(int id)
    {
        return await _departmentRepository.GetById(id);
    }

    public async Task<Department> GetByIdWithInfo(int id)
    {
        return await _departmentRepository.GetByIdWithJoin(id);
    }
    
    public async Task<Department> Add(Department entity)
    {
        return await _departmentRepository.Add(entity);
    }

    public async Task RemoveDepartment(int id)
    {
        await _departmentRepository.Delete(id);
    }

    public async Task<Department> UpdateDepartment(Department target)
    {
        return await _departmentRepository.Update(target);
    }
}
