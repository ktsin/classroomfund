﻿using ClassroomFund.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassroomFund.Services.UserManagment.Interfaces
{
    public interface IEmployeeService
    {
        public Task<List<Employee>> GetAll();
        public Task<List<Employee>> GetAllWithInformation();
        public Task<Employee> GetById(Guid id);
        public Task<Employee> GetByIdWithInformation(Guid id);
        public Task<List<EmployeeTitle>> GetAllTitles();
        Task<Employee> AddEmployee(Employee employee);
        Task<EmployeeTitle> AddEmployeeTitle(EmployeeTitle employee);
    }
}
