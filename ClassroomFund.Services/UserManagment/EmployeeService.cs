﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;
using ClassroomFund.Services.Classrooms.Interfaces;
using ClassroomFund.Services.UserManagment.Interfaces;

namespace ClassroomFund.Services.UserManagment
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IEmployeeTitleRepository _titleRepository;

        public EmployeeService(IEmployeeRepository employeeRepository, IEmployeeTitleRepository titleRepository)
        {
            _titleRepository = titleRepository;
            _employeeRepository = employeeRepository;
        }

        public async Task<List<Employee>> GetAll()
        {
            return await _employeeRepository.GetAll();
        }

        public async Task<List<Employee>> GetAllWithInformation()
        {
            return await _employeeRepository.GetAllWithJoin();
        }

        public async Task<Employee> GetById(Guid id)
        {
            return await _employeeRepository.GetById(id);
        }

        public async Task<Employee> GetByIdWithInformation(Guid id)
        {
            return await _employeeRepository.GetByIdWithJoin(id);
        }

        public async Task<List<EmployeeTitle>> GetAllTitles()
        {
            return await _titleRepository.GetAll();
        }

        public async Task<Employee> AddEmployee(Employee employee)
        {
            return await _employeeRepository.Add(employee);
        }
        
        public async Task<EmployeeTitle> AddEmployeeTitle(EmployeeTitle employee)
        {
            return await _titleRepository.Add(employee);
        }
    }
}
