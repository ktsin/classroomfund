﻿using ClassroomFund.Models;

namespace ClassroomFund.Services.Relocate;

public interface IRelocateService
{
    Task<Room> MoveToDepartment(Room room, int departmentId);
    Task<List<ClassroomMovementHistory>> GetMovementHistory();
    Task<List<ClassroomMovementHistory>> GetMovementHistoryWithInfo();
    Task<List<ClassroomMovementHistory>> GetMovementHistoryInDateRange(DateOnly from, DateOnly to);
    
}