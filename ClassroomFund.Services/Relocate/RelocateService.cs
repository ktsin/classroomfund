﻿using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;

namespace ClassroomFund.Services.Relocate;

public class RelocateService : IRelocateService
{
    private readonly IRelocationHistoryRepository _historyRepository;
    private readonly IRoomRepository _roomRepository;

    public RelocateService(IRelocationHistoryRepository historyRepository, IRoomRepository roomRepository)
    {
        _historyRepository = historyRepository;
        _roomRepository = roomRepository;
    }

    public async Task<Room> MoveToDepartment(Room room, int departmentId)
    {
        var historyRecord = new ClassroomMovementHistory()
        {
            ClassroomId = room.Id,
            EventDate = DateTime.Now,
            FromDepartment = room.DepartmentId ?? 0,
            ToDepartment = departmentId
        };
        room.DepartmentId = departmentId;
        await _historyRepository.Add(historyRecord);
        await _roomRepository.Update(room);
        return room;
    }

    public async Task<List<ClassroomMovementHistory>> GetMovementHistory()
    {
        return await _historyRepository.GetAll();
    }

    public async Task<List<ClassroomMovementHistory>> GetMovementHistoryWithInfo()
    {
        return await _historyRepository.GetAllWithJoin();
    }

    public async Task<List<ClassroomMovementHistory>> GetMovementHistoryInDateRange(DateOnly from, DateOnly to)
    {
        var fullList = await _historyRepository.GetAll();
        return fullList.AsParallel().Where(e =>
                e.EventDate >= from.ToDateTime(TimeOnly.MinValue) && e.EventDate <= to.ToDateTime(TimeOnly.MinValue))
            .OrderByDescending(e=>e.EventDate)
            .ToList();
    }
}