﻿using ClassroomFund.DAL;
using ClassroomFund.Services.Classrooms;
using ClassroomFund.Services.Classrooms.Interfaces;
using ClassroomFund.Services.Departments;
using ClassroomFund.Services.Email;
using ClassroomFund.Services.Equipment;
using ClassroomFund.Services.Equipment.Interface;
using ClassroomFund.Services.FileOperations;
using ClassroomFund.Services.FileOperations.Interfaces;
using ClassroomFund.Services.Relocate;
using ClassroomFund.Services.Reports;
using ClassroomFund.Services.UserManagment;
using ClassroomFund.Services.UserManagment.Interfaces;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ClassroomFund.Services;

public static class ServiceConfig
{
    public static IServiceCollection ConfigureServiceLayer(this IServiceCollection services, IConfiguration configuration)
    {
        services.ConfigureDataLayer(configuration);

        #region Building and etc. services

        services.AddScoped<IBuildingService, BuildingService>();
        services.AddScoped<IFloorInfoService, FloorInfoService>();
        services.AddScoped<IRoomService, RoomsService>();
        services.AddScoped<IRelocateService, RelocateService>();

        #endregion

        #region Users, Employeess and etc.

        services.AddScoped<IEmployeeService, EmployeeService>();

        #endregion

        #region Deps

        services.AddScoped<IDepartmentsService, DepartmentsService>();

        #endregion

        #region Equipment

        services.AddScoped<IEquipmentPropertiesService, EquipmentPropertiesService>();  
        services.AddScoped<IEquipmentTypeService, EquipmentTypeService>();
        services.AddScoped<IEquipmentService, EquipmentService>();
        
        #endregion

        #region Infrastructure
        
        services.AddScoped<IImageService, FirebaseImageService>();
        services.AddSingleton<IEmailSender, MainEmailService>();
        services.AddScoped<ReportsService>();
        services.AddScoped<OverviewService>();

        #endregion
        
        return services;
    }
}