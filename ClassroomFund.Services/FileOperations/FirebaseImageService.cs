﻿using System.Net;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text.RegularExpressions;
using ClassroomFund.Services.FileOperations.Interfaces;
using ClassroomFund.Services.Firebase;
using Firebase.Storage;
using Microsoft.Extensions.Configuration;

namespace ClassroomFund.Services.FileOperations;

public class FirebaseImageService : IImageService
{
    private readonly string _storageBucket;
    private readonly string FilePath = "files";

    public FirebaseImageService(IConfiguration configuration)
    {
        _storageBucket = configuration["FirebaseStorageName"];
    }

    public async Task<string> AddImage(Stream stream)
    {
        var storage = BuildFirebaseStorageClient();
        var imageObjectUrl = await storage
            .Child(FilePath)
            .Child(Guid.NewGuid().ToString("N"))
            .PutAsync(stream);
        Task.Run(async () => SetupCacheOptions(imageObjectUrl, await FirebaseUtilities.GetBearerToken()), CancellationToken.None);
        return imageObjectUrl;
    }

    public async Task RemoveImage(string location)
    {
        var storage = BuildFirebaseStorageClient();
        await storage.Child(FilePath).Child(ParseUrlForTargetLocation(location)).DeleteAsync();
    }

    private FirebaseStorage BuildFirebaseStorageClient()
    {
        return new FirebaseStorage(_storageBucket,
        new FirebaseStorageOptions()
        {
            AuthTokenAsyncFactory = FirebaseUtilities.GetBearerToken,
            HttpClientTimeout = TimeSpan.FromSeconds(30),
            ThrowOnCancel = true
        });
    }

    private string ParseUrlForTargetLocation(string url)
    {
        url = WebUtility.UrlDecode(url);
        var match = Regex.Match(url, $"^https://firebasestorage.googleapis.com/v0/b/{_storageBucket}/o/{FilePath}/*");
        if (match.Success)
        {
            Uri uri = new(url);
            var baseUri = uri.GetComponents(UriComponents.Path, UriFormat.Unescaped);
            var fileName = baseUri.Split("/").Last();
            return fileName;
        }
        return String.Empty;
    }

    private async Task SetupCacheOptions(string url, string token)
    {
        Uri baseAddress = new Uri(url);
        using HttpClient client = new HttpClient();
        client.BaseAddress = new Uri(baseAddress.GetLeftPart(UriPartial.Path));
        client.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue.Parse($"Firebase {token}");
        client.DefaultRequestHeaders.Add("Authority", "firebasestorage.googleapis.com");
        HttpContent content = JsonContent.Create(new
        {
            cacheControl = "private,max-age=604800"
        });
        await client.PatchAsync("", content);
    }
}
