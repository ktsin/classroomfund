﻿namespace ClassroomFund.Services.FileOperations.Interfaces;

public interface IImageService
{
    Task<string> AddImage(Stream stream);
    Task RemoveImage(string location);
}
