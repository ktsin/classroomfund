﻿using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Services.Classrooms.Interfaces;

namespace ClassroomFund.Services;

public class OverviewService
{
    private readonly IBuildingRepository _buildingRepository;
    private readonly IRoomRepository _roomRepository;

    public OverviewService(IBuildingRepository buildingRepository, IRoomRepository roomRepository)
    {
        _buildingRepository = buildingRepository;
        _roomRepository = roomRepository;
    }

    public async Task<UniversityOverview> GetOverview()
    {
        var result = new UniversityOverview();
        result.BuildingsNumber = (await _buildingRepository.GetAll()).Count;
        var rooms = await _roomRepository.GetAll();
        result.RoomsCount = rooms.Count;
        result.TotalArea = rooms.Select(e => e.Area ?? 0).Sum();

        return result;
    }
}

public class UniversityOverview
{
    public int BuildingsNumber { get; set; }
    
    public int RoomsCount { get; set; }
    
    public double TotalArea { get; set; }
}