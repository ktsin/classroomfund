﻿using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Configuration;
using MimeKit;

namespace ClassroomFund.Services.Email;

public class MainEmailService : IEmailSender
{
    private readonly string smtpServer;
    private readonly int port;
    private readonly bool useSecureConnection;
    private readonly string senderEmail;

    public MainEmailService(IConfiguration configuration)
    {
        IConfigurationSection section = configuration.GetSection("emailSettings");
        smtpServer = section["SMTPServer"];
        port = section.GetValue<int>("SMTPPort");
        useSecureConnection = section.GetValue<bool>("UseSSL");
        senderEmail = section["senderEmail"];
    }

    public async Task SendEmailAsync(string email, string subject, string htmlMessage)
    {
        var message = new MimeMessage();
        message.From.Add(new MailboxAddress(String.Empty, senderEmail));
        message.To.Add(new MailboxAddress(String.Empty, email));
        message.Subject = subject;

        message.Body = new TextPart("html")
        {
            Text = htmlMessage
        };
        using var client = new SmtpClient();
        await client.ConnectAsync(smtpServer, port, useSecureConnection);
        
        await client.AuthenticateAsync(senderEmail, Environment.GetEnvironmentVariable("EMAIL_PASSWORD"));

        await client.SendAsync(message);
        await client.DisconnectAsync(true);
    }
}
