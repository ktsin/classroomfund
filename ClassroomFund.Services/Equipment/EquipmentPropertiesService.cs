﻿using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;
using ClassroomFund.Services.Equipment.Interface;

namespace ClassroomFund.Services.Equipment;

public class EquipmentPropertiesService : IEquipmentPropertiesService
{
    private readonly IEquipmentPropertyRepository _equipmentPropertiesRepository;
    
    public EquipmentPropertiesService(IEquipmentPropertyRepository equipmentPropertiesRepository)
    {
        _equipmentPropertiesRepository = equipmentPropertiesRepository;
    }
    
    public async Task<List<EquipmentProperty>> GetAll()
    {
        return await _equipmentPropertiesRepository.GetAll();
    }

    public async Task<List<EquipmentProperty>> GetAllWithInfo()
    {
        return await _equipmentPropertiesRepository.GetAllWithJoin();
    }

    public async Task<EquipmentProperty> GetById(Guid id)
    {
        return await _equipmentPropertiesRepository.GetById(id);
    }

    public async Task<EquipmentProperty> GetByIdWithInfo(Guid id)
    {
        return await _equipmentPropertiesRepository.GetByIdWithJoin(id);
    }

    public async Task<EquipmentProperty> Add(EquipmentProperty entity)
    {
        return await _equipmentPropertiesRepository.Add(entity);
    }
}
