﻿using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;
using ClassroomFund.Services.Equipment.Interface;

namespace ClassroomFund.Services.Equipment;

public class EquipmentService : IEquipmentService
{
    private readonly IConcreteEquipmentRepository _repository;

    public EquipmentService(IConcreteEquipmentRepository repository)
    {
        _repository = repository;
    }

    public async Task<List<ConcreteEquipment>> GetAll()
    {
        return await _repository.GetAll();
    }

    public async Task<List<ConcreteEquipment>> GetAllWithInfo()
    {
        return await _repository.GetAllWithJoin();
    }

    public async Task<ConcreteEquipment> GetById(Guid id)
    {
        return await _repository.GetById(id);
    }

    public async Task<ConcreteEquipment> GetByIdWithInfo(Guid id)
    {
        return await _repository.GetByIdWithJoin(id);
    }

    public async Task<ConcreteEquipment> Add(ConcreteEquipment entity)
    {
        return await _repository.Add(entity);
    }

    public async Task<ConcreteEquipment> Update(ConcreteEquipment entity)
    {
        return await _repository.Update(entity);
    }

    public async Task Delete(Guid id)
    {
        await _repository.Delete(id);
    }
}