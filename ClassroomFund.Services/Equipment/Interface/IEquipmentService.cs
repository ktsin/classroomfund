﻿using ClassroomFund.Models;

namespace ClassroomFund.Services.Equipment.Interface;

public interface IEquipmentService
{
    Task<List<ConcreteEquipment>> GetAll();

    Task<List<ConcreteEquipment>> GetAllWithInfo();

    Task<ConcreteEquipment> GetById(Guid id);

    Task<ConcreteEquipment> GetByIdWithInfo(Guid id);

    Task<ConcreteEquipment> Add(ConcreteEquipment entity);

    Task<ConcreteEquipment> Update(ConcreteEquipment entity);

    Task Delete(Guid id);
}