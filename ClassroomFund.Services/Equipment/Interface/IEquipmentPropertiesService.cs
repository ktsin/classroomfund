﻿using ClassroomFund.Models;

namespace ClassroomFund.Services.Equipment.Interface;

public interface IEquipmentPropertiesService
{
    public Task<List<EquipmentProperty>> GetAll();

    public Task<List<EquipmentProperty>> GetAllWithInfo();

    public Task<EquipmentProperty> GetById(Guid id);

    public Task<EquipmentProperty> GetByIdWithInfo(Guid id);

    public Task<EquipmentProperty> Add(EquipmentProperty entity);
}
