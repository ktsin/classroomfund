﻿using ClassroomFund.Models;

namespace ClassroomFund.Services.Equipment.Interface;

public interface IEquipmentTypeService
{
    Task<List<EquipmentType>> GetAll();

    Task<List<EquipmentType>> GetAllWithInfo();

    Task<EquipmentType> GetById(Guid id);

    Task<EquipmentType> GetByIdWithInfo(Guid id);

    Task<EquipmentType> Add(EquipmentType entity);
}
