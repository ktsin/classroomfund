﻿using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;
using ClassroomFund.Services.Equipment.Interface;

namespace ClassroomFund.Services.Equipment;

public class EquipmentTypeService : IEquipmentTypeService
{
    private readonly IEquipmentTypeRepository _equipmentTypeRepository;

    public EquipmentTypeService(IEquipmentTypeRepository equipmentTypeRepository)
    {
        _equipmentTypeRepository = equipmentTypeRepository;
    }

    public async Task<List<EquipmentType>> GetAll()
    {
        return await _equipmentTypeRepository.GetAll();
    }

    public async Task<List<EquipmentType>> GetAllWithInfo()
    {
        return await _equipmentTypeRepository.GetAllWithJoin();
    }

    public async Task<EquipmentType> GetById(Guid id)
    {
        return await _equipmentTypeRepository.GetById(id);
    }

    public async Task<EquipmentType> GetByIdWithInfo(Guid id)
    {
        return await _equipmentTypeRepository.GetByIdWithJoin(id);
    }

    public async Task<EquipmentType> Add(EquipmentType entity)
    {
        return await _equipmentTypeRepository.Add(entity);
    }
}
