﻿using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;
using ClassroomFund.Services.Classrooms.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassroomFund.Services.Classrooms
{
    public class BuildingService : IBuildingService
    {
        private readonly IBuildingRepository _buildingRepository;
        private readonly IFloorInfoRepository _floorInfoRepository;

        public BuildingService(IBuildingRepository buildingRepository, IFloorInfoRepository floorInfoRepository)
        {
            _buildingRepository = buildingRepository;
            _floorInfoRepository = floorInfoRepository;
        }

        public async Task<List<Building>> GetAllBuildings()
        {
            return await _buildingRepository.GetAll();
        }

        public async Task<List<Building>> GetAllBuildingsWithInfo()
        {
            return await _buildingRepository.GetAllWithJoin();
        }

        public async Task<Building> GetBuildingById(Guid id)
        {
            return await _buildingRepository.GetById(id);
        }

        public async Task<Building> GetByIdWithInfo(Guid id)
        {
            return await _buildingRepository.GetByIdWithJoin(id);
        }

        public async Task<int> GetLastFloorNumber(Guid buildingId)
        {
            var building = await _buildingRepository.GetByIdWithJoin(buildingId);
            if (building == null)
            {
                throw new ArgumentException("Building not found", nameof(buildingId));
            }

            return building
                .FloorInfoes
                .Select(e => e.FloorNumber)
                .Distinct()
                .OrderBy(i => i)
                .LastOrDefault();
        }
    }
}
