﻿using ClassroomFund.DAL.Repository.Database;
using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;
using ClassroomFund.Services.Classrooms.Interfaces;

namespace ClassroomFund.Services.Classrooms;

public class RoomsService : IRoomService
{
    private readonly IRoomRepository _roomsRepository;
    private readonly IRoomTypeRepository _roomTypeRepository;
    private readonly IRoomTargetRepository _roomTargetRepository;
    private readonly IRoomInformationRepository _roomInformationRepository;

    public RoomsService(IRoomRepository roomsRepository,
        IRoomTypeRepository roomTypeRepository,
        IRoomTargetRepository roomTargetRepository,
        IRoomInformationRepository roomInformationRepository)
    {
        _roomInformationRepository = roomInformationRepository;
        _roomTargetRepository = roomTargetRepository;
        _roomTypeRepository = roomTypeRepository;
        _roomsRepository = roomsRepository;
    }
    
    public async Task<List<Room>> GetAll()
    {
        return await _roomsRepository.GetAll();
    }

    public async Task<List<Room>> GetAllWithInfo()
    {
        return await _roomsRepository.GetAllWithJoin();
    }

    public async Task<Room> GetById(Guid id)
    {
        return await _roomsRepository.GetById(id);
    }

    public async Task<Room> GetByIdWithInfo(Guid id)
    {
        return await _roomsRepository.GetByIdWithJoin(id);
    }

    public async Task<List<Room>> GetRoomsByFloorId(Guid floorId)
    {
        return await _roomsRepository.GetRoomsByFloorId(floorId);
    }

    public async Task<List<Room>> GetRoomsByBuildingId(Guid buildingId)
    {
        return await _roomsRepository.GetRoomsByBuildingId(buildingId);
    }

    public async Task<List<Room>> GetRoomsByDepartmentId(int departmentId)
    {
        return await _roomsRepository.GetRoomsByDepartmentId(departmentId);
    }

    public async Task<List<Room>> GetRoomsByFloorIdWithInfo(Guid floorId)
    {
        return await _roomsRepository.GetRoomsByFloorIdWithJoin(floorId);
    }

    public async Task<List<Room>> GetRoomsByBuildingIdWithInfo(Guid buildingId)
    {
        return await _roomsRepository.GetRoomsByBuildingIdWithJoin(buildingId);
    }

    public async Task<List<Room>> GetRoomsByDepartmentIdWithInfo(int departmentId)
    {
        return await _roomsRepository.GetRoomsByDepartmentIdWithJoin(departmentId);
    }

    public async Task<Room> AddRoom(Room room)
    {
        return await _roomsRepository.Add(room);
    }

    public async Task<List<RoomType>> GetRoomTypes()
    {
        return await _roomTypeRepository.GetAll();
    }

    public async Task<RoomType> AddRoomType(RoomType entity)
    {
        return await _roomTypeRepository.Add(entity);
    }
    
    public async Task<List<RoomTarget>> GetRoomTargets()
    {
        return await _roomTargetRepository.GetAll();
    }

    public async Task<RoomTarget> AddRoomTarget(RoomTarget entity)
    {
        return await _roomTargetRepository.Add(entity);
    }

    public async Task<InternalRoomProperty> GetInformationByRoomId(Guid id)
    {
        return await _roomInformationRepository.GetByRoomId(id);
    }

    public async Task<InternalRoomProperty> AddRoomInformation(InternalRoomProperty information)
    {
        return await _roomInformationRepository.Add(information);
    }
}
