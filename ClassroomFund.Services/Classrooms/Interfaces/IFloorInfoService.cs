﻿using ClassroomFund.Models;

namespace ClassroomFund.Services.Classrooms.Interfaces;

public interface IFloorInfoService
{
    Task<List<FloorInfo>> GetAll();

    Task<List<FloorInfo>> GetAllWithId();

    Task<FloorInfo> GetById(Guid id);

    Task<FloorInfo> GetByIdWithInfo(Guid id);
    
    Task<List<FloorInfo>> GetAllFloorsByBuildingId(Guid id);

    Task<FloorInfo> AddImageMapping(Guid floorId, Guid roomId, Point[] points);

    Task<FloorInfo> AddFloor(FloorInfo entity);
}
