﻿using ClassroomFund.Models;

namespace ClassroomFund.Services.Classrooms.Interfaces;

public interface IRoomService
{
    Task<List<Room>> GetAll();
    Task<List<Room>> GetAllWithInfo();
    Task<Room> GetById(Guid id);
    Task<Room> GetByIdWithInfo(Guid id);
    Task<List<Room>> GetRoomsByFloorId(Guid floorId);
    Task<List<Room>> GetRoomsByBuildingId(Guid buildingId);
    Task<List<Room>> GetRoomsByDepartmentId(int departmentId);
    Task<List<Room>> GetRoomsByFloorIdWithInfo(Guid floorId);
    Task<List<Room>> GetRoomsByBuildingIdWithInfo(Guid buildingId);
    Task<List<Room>> GetRoomsByDepartmentIdWithInfo(int departmentId);
    Task<List<RoomType>> GetRoomTypes();
    Task<RoomType> AddRoomType(RoomType entity);
    Task<List<RoomTarget>> GetRoomTargets();
    Task<RoomTarget> AddRoomTarget(RoomTarget entity);
    Task<InternalRoomProperty> GetInformationByRoomId(Guid id);
    Task<InternalRoomProperty> AddRoomInformation(InternalRoomProperty information);
    Task<Room> AddRoom(Room room);
}
