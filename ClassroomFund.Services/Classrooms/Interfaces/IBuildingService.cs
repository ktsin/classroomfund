﻿using ClassroomFund.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassroomFund.Services.Classrooms.Interfaces
{
    public interface IBuildingService
    {
        public Task<List<Building>> GetAllBuildings();

        public Task<List<Building>> GetAllBuildingsWithInfo();

        public Task<Building> GetBuildingById(Guid id);

        public Task<Building> GetByIdWithInfo(Guid id);

        public Task<int> GetLastFloorNumber(Guid buildingId);
    }
}
