﻿using System.Text.Json;
using ClassroomFund.DAL.Repository.Interfaces;
using ClassroomFund.Models;
using ClassroomFund.Services.Classrooms.Interfaces;

namespace ClassroomFund.Services.Classrooms;

public class FloorInfoService : IFloorInfoService
{
    private readonly IFloorInfoRepository _repository;

    public FloorInfoService(IFloorInfoRepository repository)
    {
        _repository = repository;
    }

    public async Task<List<FloorInfo>> GetAll()
    {
        return await _repository.GetAll();
    }

    public async Task<List<FloorInfo>> GetAllWithId()
    {
        return await _repository.GetAllWithJoin();
    }

    public async Task<FloorInfo> GetById(Guid id)
    {
        return await _repository.GetById(id);
    }

    public async Task<FloorInfo> GetByIdWithInfo(Guid id)
    {
        return await _repository.GetByIdWithJoin(id);
    }

    public async Task<List<FloorInfo>> GetAllFloorsByBuildingId(Guid id)
    {
        return await _repository.GetAllFloorsByBuildingId(id);
    }

    public async Task<FloorInfo> AddImageMapping(Guid floorId, Guid roomId, Point[] points)
    {
        var floor = await GetById(floorId);
        if (floor == null)
        {
            throw new ArgumentNullException(nameof(floorId), "Incorrect floor id");
        }

        var mappingString = floor.ClassroomToImageMapping;
        var mapping = JsonSerializer.Deserialize<List<ImageMapping>>(mappingString);
        if (!mapping?.Any(e => e.ObjectId == roomId.ToString()) ?? false)
        {
            mapping.Add(new()
            {
                ObjectId = roomId.ToString(),
                ImageBounds = points.ToList()
            });
            
        }
        else
        {
           mapping = mapping?.Where(e => e?.ObjectId != roomId.ToString()).Append(new()
           {
               ObjectId = roomId.ToString(),
               ImageBounds = points.ToList()
           }).ToList();
        }
        mappingString = JsonSerializer.Serialize(mapping);
        floor.ClassroomToImageMapping = mappingString;
        var result = await _repository.Update(floor);
        return result;
    }

    public async Task<FloorInfo> AddFloor(FloorInfo entity)
    {
        return await _repository.Add(entity);
    }
}